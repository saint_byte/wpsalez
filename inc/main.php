<?php
//=================================
add_action('wp_head', 'wp_salez_addcss');
function wp_salez_addcss()
{
echo '<link rel="stylesheet" type="text/css" href="'.plugins_url().'/wpsalez/static/css/style.css">'."\r\n";
jquery_write();
echo '<script  src="'.plugins_url().'/wpsalez/static/js/wpsalez.js" type="text/javascript"></script>'."\r\n";
if (INCLUDE_PAYSIO)
{
echo '<script src="https://paysio.com/static/v1/paysio.js"></script>'."\r\n";
echo '<link href="https://paysio.com/static/v1/paysio.css" type="text/css" rel="styleSheet" />'."\r\n";
}
}


//=================================
// This plugin main file
$p  = $_GET['p'];
$p2 = $_SERVER['REQUEST_URI'];
if (!empty($p))
{
 $uri = $p;
}
else
{
 $uri = $p2;
}
global $wpdb, $wp_query;
$request = $wp_query->request;
//==============================================
add_filter('the_content', 'wp_salez_the_content');
function wp_salez_the_content($content) {
$actions_tag = array('[WPSALEZ_PRODUCTS_LIST]','[wpsalez_product');
$a1lite_ready = false;
if (INCLUDE_A1LITE) {
        $actions_tag[] = '[wpsalez_a1lite_success]';        
                $actions_tag[] = '[wpsalez_a1lite_fail]';
                $wpsalez_a1litekey = get_option( 'wpsalez_a1litekey');
        $wpsalez_a1litesecretkey = get_option( 'wpsalez_a1litesecretkey');
                if (!empty($wpsalez_a1litekey) && !empty($wpsalez_a1litesecretkey))
                {
                $a1lite_ready = true;
                }
}

foreach($actions_tag as $tag)
{
 if (strpos($content,$tag) !== false || strpos($content,strtolower($tag)) !== false)
 {
        if ($tag == $actions_tag[0])  { ///'[WPSALEZ_PRODUCTS_LIST]'        
                        // Список продуктов
                        $cnt = '';
                        $p = new Products();
                        $products = $p->getAllProducts();
                        $cnt .= '<div class="wpsalez_product_list">';
                        foreach($products as $product)
                        {
                                $cnt .= '<div class="wpsalez_products_item">';
                                $cnt .= '<b>'.$product->name.'</b><br />';
                                $cnt .= ''.$product->descr.'<br /><br />';
                                //----------------------------------------
                                $cnt .=  get_button($product->id,$product->payment_types);
                                $cnt .= '</div>';
                                //----------------------------------------
                        }
                        $cnt .= '</div>';
                                $content = strtr($content,array($tag=>$cnt,strtolower($tag)=>$cnt));
        }
        //-----------------------------------------------------------
        if ($tag == $actions_tag[1])  { //[wpsalez_product
                do {
                $i = strpos($content,$actions_tag[1]);
                $i2 = strpos($content,']',($i+strlen($actions_tag[1])+1));
                if ($i === false) break;
                if ($i2 === false) {
                        // Очень плохо нет закрывающаего тега
                        $content = substr_replace($content,'++',$i,strlen($actions_tag[1]));
                        continue;
                }
                // Так вроде окончание есть
                $cn = trim(substr($content,
                                   ($i+strlen($actions_tag[1])+1),
                                   $i2-($i+strlen($actions_tag[1])+1)
                                   ));
                $arr = json_decode($cn,true);
                if (is_array($arr)) {
                 //Так все правильно распарсилось
                 $strdata = $cn;
                 $p = new Products();
                 $pdata = $p->getById($arr['id']);
                 if ($pdata !== false)
                 {
                        // товар есть
                        if ($arr['view'] == 'compact') {
                        // если вид компактный
                           $strdata = '<div class="wpsalez_product_list1"><div class="wpsalez_products_item">';
                           $strdata .= '<b>'.$pdata->name.'</b> ';
                           $strdata .=  get_button($pdata->id,$pdata->payment_types);
                           $strdata .= '</div></div>';
                        }
                        elseif ($arr['view'] == 'button')
                        {
                        }
                        else
                        {
                           $strdata = '<div class="wpsalez_product_list1"><div class="wpsalez_products_item">';
                           $strdata .= '<b>'.$pdata->name.'</b><br />'.$pdata->descr.'<br /><br />';
                           $strdata .=  get_button($pdata->id,$pdata->payment_types);
                           $strdata .= '</div></div>';
                        }
                 } else {
                        // Товара нет забиваем на показ
                        $strdata = '';
                 }
                } else {
                 $strdata = 'WPSALAZ_ERROR: Ошибка во вставке кода в страницу';
                }
                $content = substr_replace($content,$strdata,$i,$i2-$i+1);

                } while($i !== false);
        } //[wpsalez_product                
                if (INCLUDE_A1LITE) {                        
                        if ($tag == '[wpsalez_a1lite_fail]') {
                                $cnt = 'A1LITE';
                                $content = strtr($content,array($tag=>$cnt,strtolower($tag)=>$cnt));

                    } //[wpsalez_a1lite_fail]
            if ($tag == '[wpsalez_a1lite_success]') {
                                $cnt = '';
                                for($i=0;$i<count($_SESSION['cur_bill_id']);$i++)
                                {
                                              $b = new Bills();
                                                          $bill_id = $_SESSION['cur_bill_id'][$i];
                                                $bdata = $b->getById($bill_id );        
                                                          if ($bdata->payed == '1')
                                          {
                                                            $p = new Products();
                                                            $pdata = $p->getById($bdata->data['product_id']);
                                      $dlink = gen_download_link($bill_id ,$bdata->key);
                                        $cnt .= ''.$pdata->name.'. Ссылка для скачивания: <a href="'.$dlink.'" target="_blank">'.$dlink.'</a><br />';
                                                          }

                                }
                                $content = strtr($content,array($tag=>$cnt,strtolower($tag)=>$cnt));
            } //[wpsalez_a1lite_fail]                
       } // INCLIde_a1lite
 }
}
return $content;
}
//=================================================
if (strpos($uri,'/wpsalez/') !== false)
{
//list_hooked_functions();
       $uri_arr = explode('/',$uri);
       $text = $uri;
       foreach($uri_arr as $item)
       {
          $text .= '['.$item."]\r\n";
       }
                //==========================================================================================================
       if ($uri_arr[2] == 'a1sms')
       {
           $table       = $wpdb->prefix.'wpsalez_a1sms' ;
           $site_host = $_SERVER['HTTP_HOST'];
           $a1prefix = get_option( 'a1prefix');
           $a1seckey = get_option( 'a1seckey');
           $a1urlseckey = get_option( 'a1urlseckey');
           $a1_contact_email = get_option( 'a1contactemail');
           $date=$_GET['date'];
           $msg=$_GET['msg'];
           $msg_trans=$_GET['msg_trans'];
           $operator_id=$_GET['operator_id'];
           $clear_msg = $msg;
           $country_id=$_GET['country_id'];
           $operator=$_GET['operator'];
           $user_id=$_GET['user_id'];
           $smsid=$_GET['smsid'];
           $num=$_GET['num'];
           $cost=$_GET['cost'];
           $test=$_GET['test'];
           $try=$_GET['try'];
           $cost_rur=$_GET['cost_rur'];
           $ran=$_GET['ran'];
           $skey=$_GET['skey'];
           $sign=$_GET['sign'];
           $reqdate = date( 'Y-m-d H:i:s');
           $qstr = $_SERVER['QUERY_STRING'];
           $ip = $_SERVER['REMOTE_ADDR'];
           $uid    = 0;
           $msg_clean = strtr($msg,array($a1prefix => ''));
           $bill_id = intval(trim($msg_clean));
           
           if ($uri_arr[3] == $a1urlseckey)
           {
           // Оплачиваем счет

              $b = new Bills();
              $p = new Products();
              $bdata = $b->getById($bill_id);
              //var_dumP($bdata);
              $pdata = $p->getById($bdata->data['product_id']);
              $answer = '';
              $aswer .=  "smsid:$smsid\n";
              $aswer .= "status:reply\n";
              $aswer .=  "\n";
              if ($num != $pdata->smsnum )
              {
                $aswer .= 'Oshibka v nomer dlya otpravki. Obratits za podderzhkoi ';
              } else {
                $key = $b->setAsPayed($bill_id);
                $dlink = gen_download_link($bill_id,$key);
                $aswer .=  'S4et #'.$bill_id.' oplachen. Ska4at: '.$dlink.' .http://'.$site_host.' ';
              }
              
              if (!empty($a1_contact_email)) {
                $aswer .= 'Email:'.$a1_contact_email.' ';
              }
              $aswer .=  "\n";
           } else {
           // Так хакеры
              $answer = '';
              $aswer .=  "smsid:$smsid\n";
              $aswer .= "status:reply\n";
              $aswer .=  "\n";
              $aswer .=  'Oshibka, obratites na sait dlya podderzhki. http://'.$site_host.' ';
              if (!empty($a1_contact_email)) {
                $aswer .= 'Email:'.$a1_contact_email.' ';
              }
              $aswer .=  "\n";
           }

           //print '<pre>'; var_dump($uri_arr); print '</pre>'; die('');
           $wpdb->insert(
                              $table,
                              array(
                                    'date'    => $date,
                                    'reqdate' => $reqdate,
                                    'msg'     => $msg,
                                    'msg_trans' => $msg_trans,
                                    'clear_msg' => $clear_msg,

                                    'operator' =>$operator,
                                    'operator_id' =>$operator_id,
                                    'user_id'      => $user_id,
                                    'smsid'        => $smsid,
                                    'cost_rur'     => $cost_rur,

                                    'cost'         => $cost,
                                    'test'         => $test,
                                    'num'          => $num,
                                    'skey'         => $skey,
                                    'sign'         => $sign,

                                    'ran'          => $ran,
                                    'answer'       => $aswer,
                                    'uid'          => $uid,
                                    'ip'            => $ip,
                                    'qs'           => $qstr
                                   ),
                              array(
                                    '%s',
                                    '%s',
                                    '%s',
                                    '%s',
                                    '%s',

                                    '%s',
                                    '%d',
                                    '%s',
                                    '%d',
                                    '%f',

                                    '%f',
                                    '%d',
                                    '%s',
                                    '%s',
                                    '%s',

                                    '%d',
                                    '%s',
                                    '%d',
                                                                        '%s',
                                    '%s'
                                    )
                              );
                die($aswer);


       }  // WPSALEZ/a1sms
       //=====================================================================
       if ($uri_arr[2] == 'a1lite')
       {
           $site_host = $_SERVER['HTTP_HOST'];
           $a1urlseckey = get_option( 'a1urlseckey');
           $s = '';
           if ($uri_arr[3] == $a1urlseckey)
           {
              $bill_id = $_REQUEST['order_id'];
 
                          
                          $pa = new A1lite_Pays();
                          if ($pa->verify($_POST)) {
                                $pa->add($_POST);
                     $b = new Bills();
                      $p = new Products();
                      $bdata = $b->getById($bill_id);
                      $pdata = $p->getById($bdata->data['product_id']);
                      $b->setPhone($bill_id,$_REQUEST['phone_number']);
                      $b->setEmail( $bill_id,$_REQUEST['email']);
                      $key    = $b->setAsPayed($bill_id);
                      //------------------------------------------------
                      // Отправляем на email ссылку - на случай если оплачено через киви
                      $dlink = gen_download_link($bill_id,$key);
                      $message = 'Скачать можно по адресу:' .$dlink;
                        mail( $_POST['email'], 'Ваша ссылка на скачивание '.$site_host, $message, $headers = '' );
                      //------------------------------------------------
                      $s = 'OK';
                           } else {
                                $s = 'WRONG CHECK';
                           }
           } else {
                $s = 'WRONG URL0';
           }
           die($s);
       }
       //=====================================================================
       if ($uri_arr[2] == 'product') {
           $id               = $_GET['id'];
           $a1prefix         = get_option( 'a1prefix');
           $a1_contact_email = get_option( 'a1contactemail');
           $p = new Products();
           $pdata = $p->getById($id);
           $b = new Bills();
           $bill_id = $b->create($pdata->cost ,'','',array('product_id'=>$id));
                   
                   if (!is_array($_SESSION['cur_bill_id'])) { 
                        $_SESSION['cur_bill_id'] = array();
                   }
                   $_SESSION['cur_bill_id'][] = $bill_id;
          
                   $strdata = file_get_contents(dirname(__FILE__).'/../template/ajax_get_product.php');
           $strdata = strtr($strdata,array(
           '%A1PREFIX%' =>  $a1prefix,
           '%BILL_ID%'  =>  $bill_id,
           '%NUM%'      =>  $pdata->smsnum,
           '%SUM%'      =>  $pdata->cost,
           '%A1CONTACTEMAIL%' => $a1_contact_email
           ));
           die($strdata);
       }// WPSALEZ/product
       if ($uri_arr[2] == 'allite_product')
       {
           $id               = $_GET['id'];
           $a1litekey     = get_option( 'wpsalez_a1litekey');
           $p = new Products();
           $pdata = $p->getById($id);
           $b = new Bills();
           $bill_id = $b->create($pdata->cost ,'','',array('product_id'=>$id));

                   if (!is_array($_SESSION['cur_bill_id'])) {
                        $_SESSION['cur_bill_id'] = array();
                   }
                   $_SESSION['cur_bill_id'][] = $bill_id;

                   $strdata = file_get_contents(dirname(__FILE__).'/../template/ajax_get_product.php');
           $strdata = strtr($strdata,array(
           '%A1PREFIX%' =>  $a1prefix,
           '%BILL_ID%'  =>  $bill_id,
           '%NUM%'      =>  $pdata->smsnum,
           '%SUM%'      =>  $pdata->cost,
           '%A1CONTACTEMAIL%' => $a1_contact_email
           ));
           $url = 'https://partner.a1pay.ru/a1lite/input/?key='.urlencode($a1litekey).'&name='.urlencode($pdata->name).'&cost='.$pdata->cost.'&default_email=&order_id='.$bill_id;
           header('Location: '.$url);
           die('');
           //die($strdata);
       } // WPSALEZ/allite_product'
       //=====================================================================
       if ($uri_arr[2] == 'bill_payed') {
                $bill_id = $_GET['id'];
                $type    = $_GET['type'];
                $b = new Bills();
                $bdata = $b->getById($bill_id);
                if ($type == 'videosell')
                {
                        if (!in_array($bill_id,$_SESSION['cur_bill_id']))
                        {
                                        die('Ошибка доступа');
                        }
                        if ($bdata->payed == '1') {
                               setcookie ( 'wpz_videosell',$bill_id.'|'.$bdata->key ,(time()+(3600*24*7)), '/');
                               $strdata = 'Оплачено. <a href="javascript:location.reload()">Обновить</a>';
                        } else {
                                $strdata='';
                        }
                }
                else
                {
                        if (!in_array($bill_id,$_SESSION['cur_bill_id']))
                        {
                                        die('Ошибка доступа');                                
                        }
                        if ($bdata->payed == '1')
                        {
                                $dlink = gen_download_link($bill_id,$bdata->key);
                                $strdata = 'Оплачено. Cкачать: <a href="'.$dlink.'" target="_blank">'.$dlink.'</a>';
                        } else {
                                $strdata='';
                        }
                }
                
                die($strdata);
       }// WPSALEZ/bill_payed
       //=====================================================================
       if ($uri_arr[2] == 'download') {
                $id  = $_GET['id'];
                $key = $_GET['key'];
                $b = new Bills();
                $bdata = $b->getById($id);
                if ($key==$bdata->key)
                {

                        $p = new Products();
                        $pdata = $p->getById($bdata->data['product_id']);
                        $wpsalez_file_dir  = get_option( 'wpsalez_file_dir' );
                        if (INCLIDE_VIDEOSELL)
                        {
                                @setcookie ( 'wpz_videosell',$bill_id.'|'.$bdata->key ,(time()+(3600*24*7)), '/');
                        }
                        //@header("Content-Type: application/octet-stream");
                        //@header("Content-Transfer-Encoding: Binary");
                        //@header('Content-disposition: attachment; filename="'.$pdata->src_file.'"');
                        //die( file_get_contents($wpsalez_file_dir.'/'.$pdata->file) );
                        download_file($wpsalez_file_dir.'/'.$pdata->file, 4096, true, false);
                        die('');

                        
                } else {
                        die('FILE_ERROR');
                }

                echo readfile($url);
       }
       // WPSALEZ/download
       //=====================================================================
       if (INCLIDE_VIDEOSELL)
       {
                if ($uri_arr[2] == 'videosell') {
                        require_once dirname(__FILE__) . '/videosell/view_message.php';
                }
       }
       //-----------------------------------------------------------------------
       if (INCLIDE_PAYSIO)
       {
                if ($uri_arr[2] == 'paysio') {
                        require_once dirname(__FILE__) . '/paysio/main.php';
                }
       }
       //-----------------------------------------------------------------------

       if (ENABLE_CART)
       {
                if ($uri_arr[2] == 'add2cart') {
                        require_once dirname(__FILE__) . '/../cart/add2cart.php';
                }
                if ($uri_arr[2] == 'deleteformcart') {
                        require_once dirname(__FILE__) . '/../cart/deletefromcart.php';
                }
                if ($uri_arr[2] == 'cartbill') {
                        require_once dirname(__FILE__) . '/../cart/cartbill.php';
                }
       }
       //-----------------------------------------------------------------------
       if (INCLIDE_QIWI)
       {
                if ($uri_arr[2] == 'qiwi') {
                        if ($uri_arr[3] == 'instruction')
                        {
                                require_once dirname(__FILE__) . '/qiwi/view_instruction.php';
                        }
                        if ($uri_arr[3] == 'details_form')
                        {
                                require_once dirname(__FILE__) . '/qiwi/view_details_form.php';
                        }
                        if ($uri_arr[3] == 'end')
                        {
                                require_once dirname(__FILE__) . '/qiwi/view_end_message.php';
                        }
                        $a1urlseckey = get_option( 'a1urlseckey');
                        if ($uri_arr[3] != $a1urlseckey) {
                                die('SECURITY ERROR');
                        }
                        require_once dirname(__FILE__) . '/qiwi/view_message.php';
                }
       }
       

}


