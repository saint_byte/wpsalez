<?php
global $wpdb;
//print 'INSTALL BEGIN';
$wpsalez_file_dir  = 'wpsalez_'.base_convert ( md5(rand(0,9999).rand(0,9999).rand(0,9999).rand(0,9999)), 16 , 35 );
add_option( 'wpsalez_file_dir', $wpsalez_file_dir, '', 'yes' );
$wpsalez_file_dir = '../'.$wpsalez_file_dir;
mkdir( $wpsalez_file_dir,0755);

/*
$fl = fopen($wpsalez_file_dir.'/.htaccess','w');
fwrite($fl,'Options  -ExecCGI IncludesNOEXEC -Indexes'."\r\n");
fclose($fl);
*/

$fl = fopen($wpsalez_file_dir.'/index.html','w');
fwrite($fl,'<h1>Error 550</h1>'."\r\n");
fclose($fl);
//------------------------------------------------
$a1_url_key = md5(rand(0,9999).rand(0,9999).rand(0,9999).rand(0,9999));
add_option( 'a1key', '', '', 'yes' );
add_option( 'a1login', '', '', 'yes' );
add_option( 'a1prefix', '', '', 'yes' );
add_option( 'a1seckey', '', '', 'yes' );
add_option( 'a1contactemail', '', '', 'yes' );
add_option( 'a1urlseckey', $a1_url_key, '', 'yes' );
add_option( 'wpsalez_hide_page', true, '', 'yes' );
add_option( 'wpsalez_enable_jquery', true, '', 'yes' );
add_option( 'wpsalez_jquery_url', 'http://ajax.aspnetcdn.com/ajax/jQuery/jquery-1.8.1.min.js', '', 'yes' );
//------------------------------------------
if (INCLUDE_A1LITE) {
 add_option( 'wpsalez_a1litesecretkey', '', '', 'yes' );
 add_option( 'wpsalez_a1litekey', '', '', 'yes' );
}
//------------------------------------------
if (INCLUDE_VIDEOSELL) {
 add_option( 'wpsalez_videoselldelay', '60000', '', 'yes' );
 add_option( 'wpsalez_videosellproduct', '', '', 'yes' );
}
if (INCLUDE_PAYSIO)
{
 add_option( 'wpsalez_paysio_publickey', '', '', 'yes' );
}

$table = $wpdb->prefix.'wpsalez_a1sms';
$sql1='
CREATE TABLE IF NOT EXISTS `'.$table.'` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `date` datetime NOT NULL COMMENT "дата получения sms",
  `reqdate` varchar(128) COLLATE utf8_unicode_ci NOT NULL COMMENT "дата получения запроса сервером",
  `msg` varchar(255) COLLATE utf8_unicode_ci NOT NULL COMMENT "сообщение абонента",
  `msg_trans` varchar(255) COLLATE utf8_unicode_ci NOT NULL COMMENT "транслитерированное сообщение",
  `clear_msg` text COLLATE utf8_unicode_ci NOT NULL,
  `operator` varchar(255) COLLATE utf8_unicode_ci NOT NULL COMMENT "id оператора",
  `operator_id` int(10) unsigned NOT NULL COMMENT "id оператора",
  `user_id` bigint(20) unsigned NOT NULL COMMENT "номер абонента",
  `smsid` int(10) unsigned NOT NULL COMMENT "id смс",
  `cost_rur` float(15,4) unsigned NOT NULL COMMENT "сумма в рублях",
  `cost` float(15,4) unsigned NOT NULL COMMENT "информационный параметр (по курсу последней выплаты)", 
  `test` tinyint(1) unsigned NOT NULL COMMENT "1 - тестовая смс",
  `num` varchar(20) COLLATE utf8_unicode_ci NOT NULL COMMENT "короткий номер",
  `skey` char(32) COLLATE utf8_unicode_ci NOT NULL COMMENT "секретный ключ md5",
  `sign` char(32) COLLATE utf8_unicode_ci NOT NULL COMMENT "md5 от параметров",
  `ran` tinyint(1) unsigned NOT NULL,
  `answer` text COLLATE utf8_unicode_ci NOT NULL COMMENT "ответ сервиса без заголовка",
  `uid` int(11) NOT NULL COMMENT "ИД пользователя местной системы",
  `ip` varchar(128) COLLATE utf8_unicode_ci NOT NULL COMMENT "IP адрес",
  `qs` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `smsid` (`smsid`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1' ;
$wpdb->query($sql1);

//------------------------------------------

$table = $wpdb->prefix.'wpsalez_products';
$sql2 = 'CREATE TABLE IF NOT EXISTS `'.$table.'` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(128) COLLATE utf8_unicode_ci NOT NULL COMMENT "Name of product",
  `descr` text COLLATE utf8_unicode_ci NOT NULL COMMENT "Full Description of product",
  `cost` int(10) unsigned NOT NULL COMMENT "Цена",
  `smsnum`   varchar(255) COLLATE utf8_unicode_ci NOT NULL COMMENT "Номер для отправки смс",
  `create_date` datetime NOT NULL COMMENT "дата создания",
  `data`       text COLLATE utf8_unicode_ci NOT NULL COMMENT "Серилизованные данные",
  `payment_types`  varchar(255) COLLATE utf8_unicode_ci NOT NULL COMMENT "Типы оплаты",
  `src_file` varchar(255) COLLATE utf8_unicode_ci NOT NULL COMMENT "Исходное имя файла",
  `file`     varchar(255) COLLATE utf8_unicode_ci NOT NULL COMMENT "имя файла на диске",
  `visible` int(10) unsigned NOT NULL COMMENT "Видимость",
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=8' ;
$wpdb->query($sql2);
//------------------------------------------
$table = $wpdb->prefix.'wpsalez_bills';
$sql3 = 'CREATE TABLE IF NOT EXISTS `'.$table.'` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `dest_email` varchar(128) COLLATE utf8_unicode_ci NOT NULL COMMENT "Email",
  `dest_tel`   varchar(128) COLLATE utf8_unicode_ci NOT NULL COMMENT "Telefon",
  `data`       text COLLATE utf8_unicode_ci NOT NULL COMMENT "ответ сервиса без заголовка",
  `sum`        int(10) unsigned NOT NULL COMMENT "Цена",
  `create_date` datetime NOT NULL COMMENT "дата создания",
  `payed_date` datetime NOT NULL COMMENT "дата оплаты",
  `payed`      int(10) unsigned NOT NULL COMMENT "Оплачено",
  `key`        varchar(255) COLLATE utf8_unicode_ci NOT NULL COMMENT "Ключ для скачивания",
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=8' ;
$wpdb->query($sql3);
//------------------------------------------
if (INCLUDE_A1LITE) {
$table = $wpdb->prefix.'wpsalez_a1lite';
$sql4='
CREATE TABLE IF NOT EXISTS `'.$table.'` (
`id`         int(10) unsigned NOT NULL AUTO_INCREMENT,
`date`       datetime NOT NULL COMMENT "дата когда пришел запрос серверное время",
`tid`        varchar(128) COLLATE utf8_unicode_ci NOT NULL COMMENT "Tranzaction ID",
`name`       text COLLATE utf8_unicode_ci NOT NULL COMMENT "Название товара",
`comment`    text COLLATE utf8_unicode_ci NOT NULL COMMENT "Комментарии",
`partner_id` int(10) unsigned NOT NULL COMMENT "ID Партнера",
`service_id` int(10) unsigned NOT NULL COMMENT "ID Сервиса",
`order_id`   int(10) unsigned NOT NULL COMMENT "ID Счета в WpSalez_bills",
`type`       varchar(128) COLLATE utf8_unicode_ci NOT NULL COMMENT "Тип оплаты ",
`partner_income` varchar(128) COLLATE utf8_unicode_ci NOT NULL COMMENT "Сколько получаешь",
`system_income`  varchar(128) COLLATE utf8_unicode_ci NOT NULL COMMENT "Сколько получил а1лите",
`check`          text COLLATE utf8_unicode_ci NOT NULL COMMENT "Название товара",
`phone_number`    varchar(128) COLLATE utf8_unicode_ci NOT NULL COMMENT "Телефон  покупателя",
`email`           varchar(255) COLLATE utf8_unicode_ci NOT NULL COMMENT "Email покупателя",
`answer`         text COLLATE utf8_unicode_ci NOT NULL COMMENT "ответ сервиса без заголовка",
`uid`            int(11) NOT NULL COMMENT "ИД пользователя местной системы",
`ip`             varchar(128) COLLATE utf8_unicode_ci NOT NULL COMMENT "IP адрес",
`qs`            text COLLATE utf8_unicode_ci NOT NULL COMMENT "Query_STring польностью",
 PRIMARY KEY (`id`),
 UNIQUE KEY `tid` (`tid`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1' ;
$wpdb->query($sql4);
}
//------------------------------------------
$i = add_page('wpsalez','Мои продукты','Полный <a href="./products_list">список продуктов</a>');
add_page('products_list','Список продуктов','[wpsalez_products_list]',$i);
if (INCLUDE_A1LITE) {
        add_page('a1lite_success','A1Lite Получилось','[wpsalez_a1lite_success]',$i);
        add_page('a1lite_fail','A1Lite Ошибка','[wpsalez_a1lite_fail]',$i);
}
if (ENABLE_CART) {
        add_page('incart','Корзина','[wpsalez_cart_list]',$i);
}
// Insert the post into the database    a1lite_success a1lite_error

