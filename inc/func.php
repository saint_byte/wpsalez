<?php
function add_page($url,$title,$content,$parent_id = 0)
{
global $wpdb,$wp_query;
$post = array(
//  'ID' => [ <post id> ] //Are you updating an existing post?
//  'menu_order' => [ <order> ] //If new post is a page, sets the order should it appear in the tabs.
//  'page_template' => [ <template file> ] //Sets the template for the page.
  'comment_status' =>    'closed',  //[ 'closed' | 'open' ] // 'closed' means no comments.
 // 'ping_status' => [ ? ] //Ping status?
 // 'pinged' => [ ? ] //?
  'post_author' => 1, //The user ID number of the author.
  //'post_category' => [ array(<category id>, <...>) ] //Add some categories.
  'post_content' => $content , //The full text of the post.
  'post_date' => date('Y-m-d H:i:s'), //The time post was made.
  'post_date_gmt' => date('Y-m-d H:i:s'), //The time post was made, in GMT.
  //'post_excerpt' => [ <an excerpt> ] //For all your post excerpt needs.
  'post_name' => $url, // The name (slug) for your post post_name
  'post_parent' => $parent_id, //Sets the parent of the new post.
  //'post_password' => [ ? ] //password for post?
  'post_status' => 'publish', //[ 'draft' | 'publish' | 'pending' ] //Set the status of the new post.
  'post_title' => $title, //The title of your post.
  'post_type' => 'page' //[ 'post' | 'page' ] //Sometimes you want to post a page.
);  
$id = wp_insert_post( $post );
return $id;
}
//---------------------------------------------------------------------------------
function download_file($file = NULL, $speed_limit = 1024, $resume = true, $send_errors = false)
{ 
//return:: 0 - ok \ 1 - $file is_null \ 2 - forbidden \ 3 - 404 Error 
  if(is_null($file)){  
    return 1; 
  }else{ 
    $file_name = basename($file);  
    $speed_limit = intval($speed_limit);  
    if($speed_limit<0) $speed_limit = 1024;  

    $running_time = 0;  
    $begin_time = time();  

    set_time_limit(300);  

    if(file_exists($file)) 
    {  
      if( false !== ($file_hand = fopen($file, "rb")) ) 
      {  
        $file_size = filesize($file);  
        $file_date = date("D, d M Y H:i:s T",filemtime($file)); 
        if(preg_match("/bytes=(\d+)-/", $_SERVER["HTTP_RANGE"],$range) && $resume == true) 
        {  
          header("HTTP/1.1 206 Partial Content");  
          $offset = $file_size - intval($range[1]);  
        }else{  
          header("HTTP/1.1 200 OK");  
          $offset = 0;  
        }  

        $data_start = $offset;  
        $data_end = $file_size - 1;  
        $etag = md5($file.$file_size.$file_date);  

        fseek($file_hand, $data_start);  

        @header("Content-Disposition: attachment; filename=".$file_name);
        @header("Last-Modified: ".$file_date);  
        @header("ETag: \"".$etag."\"");  
        if($resume == true) @header("Accept-Ranges: bytes");  
        @header("Content-Length: ".($file_size-$data_start));  
        @header("Content-Range: bytes ".$data_start."-".$data_end."/".$file_size);  
        @header("Content-type: application/octet-stream");  
        //@header("Content-Transfer-Encoding: Binary");
        while(!feof($file_hand) && (connection_status()==0)) 
        {  
          print fread($file_hand,$speed_limit);  
          flush();  
          sleep(1);  
          $running_time = time() - $begin_time;  
          if($running_time>240) 
          {  
            set_time_limit(300);  
            $begin_time = time();  
          }  
        } 
        fclose ($file_hand);  
        return 0;  
      } 
      else 
      { 
        if($send_errors == true) header ("HTTP/1.0 403 Forbidden");  
        return 2;  
      } 
    }else{ 
      if($send_errors == true) header("HTTP/1.0 404 Not Found");  
      return 3;  
    }  
  } 
}  
//---------------------------------------------------------------------------------
function get_cur_http_protocol()
{
        if ($_SERVER['HTTPS']!='')
        {
                return "https://";
        } else {
                return "http://";
        }
}
//---------------------------------------------------------------------------------
function get_url_prefix()
{
 return get_cur_http_protocol().$_SERVER['HTTP_HOST'].'/wpsalez/';

}
//---------------------------------------------------------------------------------
function gen_download_link($bill_id,$key)
{
return get_url_prefix().'download/?id='.$bill_id.'&key='.$key;
}
/*
Получения ключа из кода или адреса - автоопределение
*/
function geta1litekey($data)
{
$find_s1_b = 'name=\"key\" value=\"';
$find_s1_e = '\"';
$find_s1_1_b = 'name="key" value="';
$find_s1_1_e = '"';
$find_s2_b = 'key=';
$find_s2_e = '&';

$i = strpos($data,$find_s2_b);
if ($i !== false) 
{
 // Парсим ссылку
 $i = strpos($data,$find_s2_b);
 $i2 = strpos($data,$find_s2_e,$i+strlen($find_s2_b));
 if ($i !== false && $i2 !== false)
 {
        $key = substr($data,$i+strlen($find_s2_b), $i2 - ($i+strlen($find_s2_b)));
        $key = urldecode($key);
        return $key;
 } else {
        return '';
 }
} else { 
// Парсим код формы
        $i = strpos($data,$find_s1_b);
        if ($i === false) 
        {
                $i = strpos($data,$find_s1_1_b);
                $find_s1_b = $find_s1_1_b;
                $find_s1_e = $find_s1_1_e;
        }
        if ($i !== false) {
                $i2 = strpos($data,$find_s1_e,$i+strlen($find_s1_b));
                if (i2 !== false) {
                        $key = substr($data,$i+strlen($find_s1_b), $i2 - ($i+strlen($find_s1_b)));
                        return $key;
                } else {
                        return ''; 
                }
                
        } else {
                return '';
        }
} 
}
//----------------------------------------------------------------------------------
function fix_magic_quotes_gpc($ignore_ini_settings = false )
{
if (get_magic_quotes_gpc() || $ignore_ini_settings) {
    $process = array(&$_GET, &$_POST, &$_COOKIE, &$_REQUEST);
    while (list($key, $val) = each($process)) {
        foreach ($val as $k => $v) {
            unset($process[$key][$k]);
            if (is_array($v)) {
                $process[$key][stripslashes($k)] = $v;
                $process[] = &$process[$key][stripslashes($k)];
            } else {
                $process[$key][stripslashes($k)] = stripslashes($v);
            }
        }
    }
    unset($process);
}
}
//-----------------------------------------------------------------------------
function jquery_write()
{
$wpsalez_enable_jquery = get_option( 'wpsalez_enable_jquery');
if ($wpsalez_enable_jquery)
{
        $wpsalez_jquery_url    = get_option( 'wpsalez_jquery_url');
        if ( $wpsalez_jquery_url == '')
        {
                $wpsalez_jquery_url = 'http://ajax.aspnetcdn.com/ajax/jQuery/jquery-1.8.1.min.js';
        }
        echo '<script  src="'.$wpsalez_jquery_url.'" type="text/javascript"></script>'."\r\n";
}
//wp_enqueue_script('jquery');
}
//---------------------------------------------------------------------------
function GetInTranslit($string) {
        $string=strtr($string,"абвгдеёзийклмнопрстуфхцыэАБВГДЕЁЗИЙКЛМНОПРСТУФХЦЫЭ","abvgdeeziyklmnoprstufhcieABVGDEEZIKLMNOPRSTUFHCIE");
        $string=strtr($string, array(
                "ж"=>"zh", " "=>"-", "ч"=>"ch", "ш"=>"sh",
                "щ"=>"shch", "ь"=>"", "ъ"=>"", "ю"=>"yu", "я"=>"ya",
                "Ж"=>"Zh", "Ч"=>"Ch", "Ш"=>"Sh",
                "Щ"=>"Shch", "Ь"=>"", "Ъ"=>"", "Ю"=>"Yu", "Я"=>"Ya",
                "і"=>"i", "І"=>"I", "ї"=>"yi", "Ї"=>"Yi", "є"=>"ie", "Є"=>"Ye")
        );
return $string;
}

//------------------------------------------------------------------------
function get_button($id,$pt_arr, $bill_os_ready = false)
{
        $cnt = '';
        if (count($pt_arr) == 1)
        {
            $BUTT_IMG_OFF = true;
        } else
        {
           $BUTT_IMG_OFF = false;
        }
        //--------------------------
        // Тут принудительно включать если есть настройки
        if (INCLIDE_A1SMS)
        {
                @include_once(dirname(__FILE__).'/../template/button/a1sms.php');
                if (function_exists('button_a1sms_make') && in_array('a1sms',$pt_arr))
                {
                        //$cnt .= 'a1sms';
                        $cnt .= button_a1sms_make($id,$BUTT_IMG_OFF,$bill_os_ready);
                }
        }
        if (INCLIDE_A1LITE)
        {
                @include_once(dirname(__FILE__).'/../template/button/a1lite.php');
                if (function_exists('button_a1lite_make') && in_array('a1lite',$pt_arr))
                {
                        //$cnt .= 'a1lite';
                        $cnt .= button_a1lite_make($id,$BUTT_IMG_OFF,$bill_os_ready);
                }
        }
        if (INCLIDE_QIWI)
        {
                @include_once(dirname(__FILE__).'/../template/button/qiwi.php');
                 if (function_exists('button_qiwi_make') && in_array('qiwi',$pt_arr))
                {
                        //$cnt .= 'qiwi';
                        $cnt .= button_qiwi_make($id,$BUTT_IMG_OFF,$bill_os_ready);
                }
        }

        if (INCLIDE_PAYSIO)
        {
                @include_once(dirname(__FILE__).'/../template/button/paysio.php');
                 if (function_exists('button_paysio_make') && in_array('paysio',$pt_arr))
                {
                        //$cnt .= 'qiwi';
                        $cnt .= button_paysio_make($id,$BUTT_IMG_OFF,$bill_os_ready);
                }
        }

        return $cnt;
}
