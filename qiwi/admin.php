<?php
    $SUBMENU[] = array(
                    'title'      => 'WP SaleZ',
                    'menu'       => 'Настройка qiwi',
                    'capability' => 8,
                    'menu_slug'  => 'qiwisettings',
                    'fnc'        => 'wpz_qiwi_settings'
                  );
    
    function wpz_qiwi_settings()
    {
        //$wpsalez_qiwi_login =    qiwi_login
        //wpsalez_qiwi_billttl =     qiwi_billttl
        //$wpsalez_qiwi_password
        $email_file =  dirname(__FILE__).'/../inc/qiwi/data/email.html';
        if ($_POST) {
         $wpsalez_qiwi_email_text = $_POST['wpsalez_qiwi_email_text'];
         file_put_contents( $email_file,$wpsalez_qiwi_email_text);
         update_option( 'wpsalez_qiwi_login',    $_POST['wpsalez_qiwi_login']);
         if ($_POST['wpsalez_qiwi_password'] != '') {
                update_option( 'wpsalez_qiwi_password', $_POST['wpsalez_qiwi_password']);
         }
         update_option( 'wpsalez_qiwi_billttl',  $_POST['wpsalez_qiwi_billttl']);
         update_option( 'wpsalez_qiwi_mailsubj',  $_POST['wpsalez_qiwi_mailsubj']);
        }
        $wpsalez_qiwi_login     = get_option( 'wpsalez_qiwi_login');
        $wpsalez_qiwi_password  = get_option( 'wpsalez_qiwi_password');
        $wpsalez_qiwi_billttl   = get_option( 'wpsalez_qiwi_billttl');
        $wpsalez_qiwi_mailsubj  = get_option( 'wpsalez_qiwi_mailsubj');
        $wpsalez_qiwi_email_text = file_get_contents($email_file);

        include(dirname(__FILE__).'/../template/qiwi/settings.php');
    }
