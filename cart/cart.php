<?php
function wpsalez_cart_list_func($atts) {
     extract(shortcode_atts(array(
     ), $atts));
     $s = '';
     $C = new Cart();
     $s .= '<br />';
     $s .= '<table class="wpsalez_cart_cart_list">';
     $products = $C->getAll();
     if (count($products) > 0 ) {
     $view_products = array();
     foreach($products as $id => $cnt)
     {

     $s .= '<tr class="wpsalez_cart_tr" >';
     $pr = New Products();
     $data = $pr->getById($id);
     $s .= '<td class="wpsalez_cart_name">'.$data->name.'</td>';
     $s .= '<td class="wpsalez_cart_cost">'.$data->cost.'</td>';
     $s .= '<td class=""><a href="#" class="wpsalez_cart_delete_link" onclick="wpsalez_delete_form_cart('.$id.')">Удалить</a></td>';
     $s .= '</tr>';
     }
     $s .= '<tr class="wpsalez_cart_tr" >';
     $s .= '<td class="">Итого:</td>';
     $s .= '<td class="wpsalez_cart_cost_itogo"></td>';
     $s .= '<td class=""> </td>';
     $s .= '</tr>';
     $s .= '</table>';
     $s .= '<br />';
     $s .= '<a href="#" onclick="wpsalez_cart_bill()" class="wpsalez_cart_bill_link">Выставить счет</a>';
     $s .= '<br />';
     $s .= '<script>'."\r\n";
     $s .= 'jQuery(document).ready(function() { wpsalez_cart_itogo(); });'."\r\n";
     $s .= '</script>'."\r\n";
     } else
     {
      $s = 'Корзина пуста';
     }
     return $s;
}
add_shortcode('wpsalez_cart_list', 'wpsalez_cart_list_func');
//=======================================================
function wpsalez_cart_add_item_func($atts) {
     extract(shortcode_atts(array(
      'id' => 0
     ), $atts));
     $BUTT_IMG_OFF = true;
     if ($id  > 0 ) {
        @include_once(dirname(__FILE__).'/../template/button/cart.php');
        $C = new Cart();
        $s = button_cart_make($id,$BUTT_IMG_OFF,$C->itemExists($id));
        return $s;
     }
     return '';
}
add_shortcode('wpsalez_cart_add_item', 'wpsalez_cart_add_item_func');
//=======================================================

class WPSalez_Widget extends WP_Widget {

        /**
         * Register widget with WordPress.
         */
        public function __construct() {
                parent::__construct(
                         'wpsalez_cart_widget', // Base ID
                        'WPSalez Cart', // Name
                        array( 'description' => __( 'Корзина WPSalez', 'text_domain' ), ) // Args
                );
        }

        /**
         * Front-end display of widget.
         *
         * @see WP_Widget::widget()
         *
         * @param array $args     Widget arguments.
         * @param array $instance Saved values from database.
         */
        public function widget( $args, $instance ) {
                extract( $args );
                $title = apply_filters( 'widget_title', $instance['title'] );

                echo $before_widget;
                if ( ! empty( $title ) )
                        echo $before_title . $title . $after_title;
                echo __( '<a href="/wpsalez/incart/" class="wpsalez_cart_link">Корзина</a>', 'text_domain' );
                echo $after_widget;
        }

        /**
         * Sanitize widget form values as they are saved.
         *
         * @see WP_Widget::update()
         *
         * @param array $new_instance Values just sent to be saved.
         * @param array $old_instance Previously saved values from database.
         *
         * @return array Updated safe values to be saved.
         */
        public function update( $new_instance, $old_instance ) {
                $instance = array();
                $instance['title'] = strip_tags( $new_instance['title'] );

                return $instance;
        }

        /**
         * Back-end widget form.
         *
         * @see WP_Widget::form()
         *
         * @param array $instance Previously saved values from database.
         */
        public function form( $instance ) {
                if ( isset( $instance[ 'title' ] ) ) {
                        $title = $instance[ 'title' ];
                }
                else {
                        $title = __( 'Корзина', 'text_domain' );
                }
                ?>
                <p>
                <label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:' ); ?></label>
                <input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
                </p>
                <?php
        }

} // class WPSalez_Widget
function widgets_init_register_widget()
{
return register_widget( 'WPSalez_Widget' );
}
add_action( 'widgets_init', 'widgets_init_register_widget');

