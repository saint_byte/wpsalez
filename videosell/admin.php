<?php
$SUBMENU[] =array(
                'title'      => 'WP SaleZ',
                'menu'       => 'Продажа видео',
                'capability' => 8,
                'menu_slug'  => 'videosell',
                'fnc'        => 'wpz_videosell'
        );
        
$SUBMENU[] =array(
                'title'      => 'WP SaleZ',
                'menu'       => 'Продажа видео / код для вставки',
                'capability' => 8,
                'menu_slug'  => 'videosellgetcode',
                'fnc'        => 'wpz_videosellgetcode'
        );

//=======================================================

function wpz_videosell()
{
$p = new Products();
$products = $p->getNamedArray();
if ($_POST)
{
        $msg ='';
        $wpsalez_videoselldelay = intval($_POST['wpsalez_videoselldelay']);
        if ($wpsalez_videoselldelay > 0)
        {
         $wpsalez_videoselldelay = $wpsalez_videoselldelay*1000;
         update_option( 'wpsalez_videoselldelay', $wpsalez_videoselldelay);
        }
        $wpsalez_videosellproduct = intval($_POST['wpsalez_videosellproduct']);
        update_option( 'wpsalez_videosellproduct', $wpsalez_videosellproduct);
}
$wpsalez_videoselldelay   = intval(get_option( 'wpsalez_videoselldelay')) / 1000;
$wpsalez_videosellproduct = get_option( 'wpsalez_videosellproduct');
include(dirname(__FILE__).'/../template/videosell/index.php');
}

//=======================================================

function wpz_videosellgetcode()
{
include(dirname(__FILE__).'/../template/videosell/codeform.php');
}
