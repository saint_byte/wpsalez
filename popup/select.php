<?php
/**
 * Manage media uploaded file.
 *
 * There are many filters in here for media. Plugins can extend functionality
 * by hooking into the filters.
 *
 * @package WordPress
 * @subpackage Administration
 */

if ( ! isset( $_GET['inline'] ) )
        define( 'IFRAME_REQUEST' , true );

/** Load WordPress Administration Bootstrap */
require_once('../../../../wp-admin/admin.php');
require_once('../inc/func.php');
require_once('../classes/products.php');
$p = new Products();

if (!current_user_can('upload_files'))
        wp_die(__('You do not have permission to upload files.'));

wp_enqueue_script('plupload-handlers');
wp_enqueue_script('jquery');
@header('Content-Type: ' . get_option('html_type') . '; charset=' . get_option('blog_charset'));
$allp = $p->getAllProducts();

?><!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=<?php echo get_option('blog_charset'); ?>" />
<? jquery_write(); ?>
<link rel="stylesheet" type="text/css" href="<?php  echo plugins_url(); ?>/wpsalez/static/css/style_select_popup.css">
<script src="<?php  echo plugins_url(); ?>/wpsalez/static/js/mce_popup.js"></script>
</head>
<body>
<table>
<tr>
<td>Продукт:</td>
<td>
<select id="product" style="width:150px;">
<?php
foreach( $allp as $pp)
{
print '<option value="'.$pp->id.'">'.$pp->name.'</option>';
}
?>
</select>
</td>
</tr>
<tr>
<td>
Вид:
</td>
<td>
<select id="view_type"  style="width:150px;">
<option value="default">С полным описанием продукта</option>
<option value="compact">Компактный вид</option>
</select>
</td>
</tr>
<tr>
<td colspan="2">
<input type="button" value="Вставить" id="insbutt"> <input type="button" value="Вставить кнопку 'В корзину'" id="cartinsbutt">
</td>
</tr>
</table>
