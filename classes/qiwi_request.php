<?php
class Qiwi_Request
{
        private $db;
        private $table;
        private $password;
        private $login;
        private $billttl;
        private $remote_service_url;
        public function __construct()
        {
                global $wpdb, $wp_query;
                $this->remote_service_url = 'http://ishop.qiwi.ru/xml';
                $this->login    = get_option( 'wpsalez_qiwi_login');
                $this->password = get_option( 'wpsalez_qiwi_password');
                $this->billttl  = get_option( 'wpsalez_qiwi_billttl');
                
                //https:/ /mylk.qiwi.ru/services/ishop
                //$this->db = $wpdb;
                //$this->table   = $wpdb->prefix.'wpsalez_a1lite' ;
        }
        
        private function request($data,$header = array(),$post = true)
        {
        if (count($header) == 0)
        {
         $header = array('Content-Type: text/plain');
        }
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,            $this->remote_service_url );
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type' => 'text/xml; encoding=utf-8'));
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        if ($post)
        {
                curl_setopt($ch, CURLOPT_POST,           1 );
        }
        curl_setopt($ch, CURLOPT_POSTFIELDS,     $data);
        curl_setopt($ch, CURLOPT_HTTPHEADER,     $header);
        $result=curl_exec ($ch);
        curl_close($ch);
        return $result;
        }

        public function createBill($bill_id,$to,$amount,$comment = '')
        {
        $xml_data = '<'.'?'.'xml version="1.0" encoding="utf-8"'.'?'.'>'."\r\n";
        $xml_data .= '<request>'."\r\n";
        $xml_data .= '<protocol-version>4.00</protocol-version>'."\r\n";
        $xml_data .= '<request-type>30</request-type>'."\r\n";
        $xml_data .= '<extra name="password">'.$this->password.'</extra>'."\r\n";
        $xml_data .= '<terminal-id>'.$this->login.'</terminal-id>'."\r\n";
        $xml_data .= '<extra name="comment">'.$comment.'</extra>'."\r\n";
        $xml_data .= '<extra name="to-account">'.$to.'</extra>'."\r\n";
        $xml_data .= '<extra name="amount">'.$amount.'</extra>'."\r\n";
        $xml_data .= '<extra name="txn-id">'.$bill_id.'</extra>'."\r\n";
        $xml_data .= '<extra name="ALARM_SMS">0</extra>'."\r\n";
        $xml_data .= '<extra name="create-agt">1</extra>'."\r\n";
        $xml_data .= '<extra name="ACCEPT_CALL">0</extra>'."\r\n";
        $xml_data .= '<extra name="ltime">'.$this->billttl.'</extra>'."\r\n";
        $xml_data .= '</request>';
        //print '<textarea>'.$xml_data.'</textarea>';
        return $this->request($xml_data);
        }

        
}
