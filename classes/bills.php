<?php
class Bills
{
        private $db;
        private $table;
        public function __construct()
        {
                global $wpdb, $wp_query;
                $this->db = $wpdb;
                $this->table = $wpdb->prefix.'wpsalez_bills';
        }
        public function create($sum = 0 ,$dest_email = '',$dest_tel='',$data = array())
        {
        //`id`,`dest_email`,`dest_tel`,`data`,`sum`,`create_date`,`payed_date`,`payed`
        $data = json_encode($data);
          $id =  $this->db->insert(
                              $this->table,
                              array(
                                    'dest_email' => $dest_email,
                                    'dest_tel'   => $dest_tel,
                                    'data'       => $data,
                                    'sum'        => $sum,

                                    'create_date' =>date( 'Y-m-d H:i:s'),
                                    'payed_date' =>'0000-00-00 00:00:00',
                                    'payed'      => '0'
                                   ),
                              array(
                                    '%s',
                                    '%s',
                                    '%s',
                                    '%s',

                                    '%s',
                                    '%s',
                                    '%d'
                                    )
                              );
                              return $this->db->insert_id;
        }
        public function getById($id)
        {
         $bill = $this->db->get_results('SELECT * FROM '.$this->table.' WHERE id='.$id.'');
         if ($bill[0]->data !='')
         {
                        $bill[0]->data = json_decode($bill[0]->data,true);
         }

                   if ($bill[0]) {
                        return $bill[0];
                  } else {
                        return false;
                  }
        }
                //---------------------------------------------
                public function setPhone($id,$dest_tel )
                {
                if (empty($dest_tel)) return false;
                if (intval($id) == 0) return false;
        $this->db->update($this->table, array(
                                              'dest_tel' =>$dest_tel ,
                                              ),
                                        array( 'id' => $id ),
                                        array( '%s'),
                                        array( '%d' )
                                        );                
                return true;        
                }

                //---------------------------------------------
                public function setEmail($id,$email)
                {
                if (empty($email)) return false;
                if (intval($id) == 0) return false;
        $this->db->update($this->table, array(
                                              'dest_email' =>$email,
                                              ),
                                        array( 'id' => $id ),
                                                                                array( '%s'),
                                        array( '%d' )
                                        );                
                return true;                                                                
                }
                //---------------------------------------------
        public function setAsPayed($id)
        {
        $key=base_convert(md5(uniqid(rand(1,10000))),16,36);
        $this->db->update($this->table, array(
                                              'payed_date' =>date( 'Y-m-d H:i:s'),
                                              'payed'      => '1',
                                              'key'        => $key
                                              ),
                                        array( 'id' => $id ),
                                        array( '%s',  '%d','%s' ),
                                        array( '%d' )
                                        );
          return $key;
        }
                //---------------------------------------------
        public function getAllBills()
        {
                   $bills = $this->db->get_results(
        "
        SELECT *
        FROM ".$this->table."
        ORDER BY create_date DESC
        "
  );     return $bills;
        }
        //----------------------------------------------
        public function getbyIdAndKey($id,$key)
        {
           if (intval($id) == 0) return false;
           if ($key == '') return false;
           $data = $this->getById($id);
           if ($key = $data->key)
           {
                return $data;
           }
           else {
                return false;
           }
        }
}
