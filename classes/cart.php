<?php
function array_remove_key ($arr,$key)
{
 $res = array();
 foreach($arr as  $k => $v)
 {
  if ($k == $key)
  {
   continue;
  }
  $res[$k] = $v;
 }
 return $res;
}

class Cart
{
        private $_ids;
        public function __construct()
        {
                $this->_ids = @$_SESSION['wpsalez_cart'];
        }
        public function add($id)
        {
                $this->_ids[$id] = 1;
                $_SESSION['wpsalez_cart'][$id] = 1;
        }
        public function remove($id)
        {

                //$this->_ids = array_remove_key ($this->_ids,$id);
                //$_SESSION['wpsalez_cart'] = array_remove_key ( $_SESSION['wpsalez_cart'],$id);
                unset($this->_ids[$id]);
                unset($_SESSION['wpsalez_cart'][$id]);
        }
        public function getAll()
        {
                if (count($this->_ids) == 0) return array();
                return $this->_ids;
        }
        public function itemExists($id)
        {
                $arr = array_keys($this->getAll());
                if (in_array($id,$arr)) return true;
                else return false;
        }
        public function clear()
        {
        $this->_ids=array();
        $_SESSION['wpsalez_cart'] = array();
        }
}
