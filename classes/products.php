<?php
class Products
{
        private $db;
        private $table;
        public $paytypes;
        public function __construct()
        {
                global $wpdb, $wp_query;
                $this->db = $wpdb;
                $this->table = $wpdb->prefix.'wpsalez_products';
                if (INCLIDE_A1SMS)
                {
                }
                if (INCLIDE_A1LITE)
                {
                }
                if(INCLIDE_QIWI)
                {
                }
        }
        //===================================================================
        public function add($data)
        {
                $name        = $data['name'];
                $description = $data['description'];
                $cost        = $data['cost'];
                $smsnum      = $data['smsnum'];
                $visible     = '1';
                                $datadata = json_encode($data['data']);
                                $payment_types = json_encode($data['payment_types']);
                //------------------------------------------------------
                if ($_FILES['file']) {
                        $wpsalez_file_dir  = get_option( 'wpsalez_file_dir' );
                        $wpsalez_file_dir  = '../'.$wpsalez_file_dir ;

                        $src_filename = basename($_FILES['file']['name']);
                        $path_parts = pathinfo($_FILES['file']['name']);

                        $dest_filename = md5_file($_FILES['file']['tmp_name']).'.'.$path_parts['extension'];
                        move_uploaded_file($_FILES['file']['tmp_name'],$wpsalez_file_dir.'/'. $dest_filename );
                        $visible = '1';
                } else {
                        return '';
                }
                //------------------------------------------------------
                if (count($data['paytype']) > 0)
                {
                        $payment_types = join(',',$data['paytype']);
                } else {
                        return '';
                }
                //------------------------------------------------------
                $this->db->insert(
                              $this->table,
                              array(
                                    'name' => $name,
                                   'descr' => $description,
                                    'cost' => $cost,
                                  'smsnum' => $smsnum,
                             'create_date' => date( 'Y-m-d H:i:s'),
                                                         
                                                                        'data' => $datadata,
                                                                        'payment_types' => $payment_types,
                                'src_file' => $src_filename,
                                    'file' => $dest_filename,
                                 'visible' => $visible
                                   ),
                              array(
                                    '%s',
                                    '%s',
                                    '%d',
                                    '%s',
                                    '%s',
                                    
                                    '%s',
                                    '%s',
                                    '%s',
                                    '%s',
                                    '%d',
                                    )
                              );
                 return true;

        }
        //===================================================================
        public function save($data,$id)
        {                $name        = $data['name'];                
                         $description = $data['description'];
                         $cost        = $data['cost'];
                         $smsnum      = $data['smsnum'];
                         $datadata = json_encode($data['data']);
                         //----------------------------
                         if (count($data['paytype']) > 0)
                         {
                           $payment_types = join(',',$data['paytype']);
                         } else {
                           return '';
                         }
                          //----------------------------
                                                  $visible     = '1';
          $this->db->update(
                              $this->table,
                              array(
                                    'name' => $name,
                                   'descr' => $description,
                                    'cost' => $cost,
                                  'smsnum' => $smsnum,
                                                                        'data' => $datadata,
                                                                        
                                                                        'payment_types' => $payment_types ,
                                 'visible' => $visible
                                   ),
                              array(
                                    'id'=>$id,
                                    ),
                              array(
                                    '%s',
                                    '%s',
                                    '%d',
                                    '%s',
                                    '%s',                                                                                
                                                                        
                                    '%s',
                                    '%d',

                                  
                                    ),

                               array(
                                    '%d'
                                    )
                              );
                 return true;
        }
        //===================================================================
        public function del($id)
        {
         $pdata = $this->getById($id);
         $wpsalez_file_dir  = get_option( 'wpsalez_file_dir' );
         $wpsalez_file_dir  = '../'.$wpsalez_file_dir ;
         @unlink( $wpsalez_file_dir.'/'.$pdata->file);
         
         $this->db->query('DELETE FROM '.$this->table.' WHERE id = "'.intval($id).'"');

        }
        //===================================================================
        public function getNamedArray()
        {
           $products = $this->db->get_results('SELECT `id`,`name` FROM '.$this->table.' ORDER BY `id`');
           foreach($products as $product)
           {
                $res[$product->id] = $product->name;
           }
           return $res;
        }
        //===================================================================
        public function getAllProducts()
        {
           $products = $this->db->get_results(
        "
        SELECT *
        FROM ".$this->table."
        ORDER BY create_date DESC
        "
        );
        for($i = 0 ;$i < count($products);$i++)
        {
           $payment_types = $products[$i]->payment_types;
           $payment_types = explode(',', $payment_types);
           $products[$i]->payment_types = $payment_types;
        }
        return $products;
        }
                //======================================================================
                public function getById($id)
                {
                   $product = $this->db->get_results('SELECT * FROM '.$this->table.' WHERE id='.$id.'');

                   if ($product) {
                   
                        $payment_types = $product[0]->payment_types;
                        $payment_types = explode(',', $payment_types);
                        //var_dump($payment_types);
                        $product[0]->payment_types = $payment_types;
                        
                        return $product[0];
                  } else { 
                        return false; 
                  }
                }
}
