<?php
class Groups
{
        private $db;
        private $table;
        public function __construct()
        {
                global $wpdb, $wp_query;
                $this->db = $wpdb;
                $this->table   = $wpdb->prefix.'wpsalez_groups' ;
        }
        private function filter_arr2($data_arr)
        {
                for($i=0;$i<count($data_arr); $i++)
                {
                        $ii = intval($data_arr[$i]);
                        if ($ii == 0 )
                        {
                         array_splice($data_arr,$i,1);
                        }
                        else
                        {
                          $data_arr[$i] = $ii;
                        }
                }
                return $data_arr;
        }
        
        public function add($name,$data_arr)
        {
                $data_arr = $this->filter_arr2($data_arr);
                $datadata = json_encode(array('products' => $data_arr));
                $this->db->insert(
                                   $this->table,
                                   array(
                                       'name' => $name,
                                       'products' => $datadata,
                                       'date' =>date( 'Y-m-d H:i:s'),
                                        ),
                                   array (
                                        '%s',
                                        '%s',
                                        '%s'
                                        )
                                 );
                //die(mysql_error());
                return true;
        }
        public function save($id,$name,$data_arr)
        {
                $data_arr = $this->filter_arr2($data_arr);
                $datadata = json_encode(array('products' => $data_arr));
                 $this->db->update(
                                   $this->table,
                                   array(
                                       'name' => $name,
                                       'products' => $datadata
                                        ),
                                   array( 'id' => $id ),
                                   array (
                                        '%s',
                                        '%s'
                                        ),
                                   array( '%d')
                                 );
        }
        public function getById($id)
        {
         $dt = $this->db->get_results('SELECT * FROM '.$this->table.' WHERE id='.$id.'');
         if ($dt)
         {
                $dt[0]->products = json_decode($dt[0]->products,true);
                return $dt[0];
         }
         else
         {
                return false;
         }
         
        }
        public function getAll()
        {
        $all = $this->db->get_results(
        "
        SELECT *
        FROM ".$this->table."
        ORDER BY date DESC
        "
        );
        return $all;
        }



}

