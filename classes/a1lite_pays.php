<?php 
class A1lite_Pays
{
      	 private $db;
        private $table;
        public function __construct()
        {
                global $wpdb, $wp_query;
                $this->db = $wpdb;
                $this->table   = $wpdb->prefix.'wpsalez_a1lite' ;
        }
	 public function verify($t)
	 {
		$secret = get_option( 'wpsalez_a1litesecretkey');
        $params = array(  'tid'            =>  $t['tid'],
        	           'name'           =>  $t['name'], 
                          'comment'        =>  $t['comment'],
                          'partner_id'     =>  $t['partner_id'],
                          'service_id'     =>  $t['service_id'],
                          'order_id'       =>  $t['order_id'],
                          'type'           =>  $t['type'],
                          'partner_income' =>  $t['partner_income'],
                          'system_income'  =>  $t['system_income']
                       );         
		if ($t['test'] == '1') {  $params['test'] = '1'; } 
 		$params['check'] = md5(join('', array_values($params)) . $secret);
		if ($params['check']  === $t['check'])
		{
			//die($params['check'].'  === '.$t['check']);
			return true;

		}	
		else 
		{
			return false;
		}
	 }
//---------------------------------------------
	 public function add($data)
	 { 
         //`id`, `date`, `tid`, `name`, `comment`, `partner_id`, `service_id`, `order_id`, `type`, `partner_income`, `system_income`, `check`, `phone_number`, `email`, `answer`, `uid`, `ip`, `qs`
         $tid     = $data['tid'];
		 $uid     = 0;
         $aswer = '';
		 $reqdate = date( 'Y-m-d H:i:s');
         $qstr =   json_encode($_POST);
         $ip = $_SERVER['REMOTE_ADDR'];
         $this->db->insert(
                              $this->table,
                              array(
                                    'date'    => $reqdate,
                                    'tid'     => $tid ,
                                    'name'    => $data['name'],
                                    'comment' => $data['comment'],
									'partner_id' => $data['partner_id'],
									
									'service_id' => $data['service_id'],
									'order_id'   => $data['order_id'],
                                    'type'       => $data['type'],
									'partner_income' =>  $data['partner_income'],
									'system_income'  =>  $data['system_income'],

                                    'check'        => $data['check'],
                                    'phone_number' => $data['phone_number'],
                                    'email'        => $data['email'],
                                    'answer'       => $aswer,
                                    'uid'          => $uid,

                                    'ip'            => $ip,
                                    'qs'            => $qstr
                                   ),
                              array(
                                    '%s',
                                    '%s',
                                    '%s',
                                    '%s',
                                    '%d',

                                    '%d',
                                    '%d',
                                    '%s',
                                    '%s',
                                    '%s',

                                    '%s',
                                    '%s',
                                    '%s',
                                    '%s',
                                    '%d',

                                    '%s',
                                    '%s'
                                    )
                              );

	 } // function add
}