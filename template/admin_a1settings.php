<?php /*
Название:
URL страницы, на которой предлагается оплатить услугу:
URL скрипта обработчика на вашем сайте:
URL дополнительного скрипта обработчика:
Тип сервиса:
Префиксы:
Секретный ключ:
Кодировка ответа сервиса:

https://partner.a1pay.ru/service/index
*/?>
<div class="wrap">
<h2>WP Salez / Настройка a1sms</h2>
<form method="Post">
<table class="form-table">
<tbody>
<tr>
                <th>
                    <label for="name">URL страницы, на которой предлагается оплатить услугу:</label>
                </th>
                <td>
          http://<?php echo $_SERVER['HTTP_HOST']; ?>/wpsalez/products_list
          </td>
</tr> 
<tr>
                <th>
                    <label for="name">URL скрипта обработчика на вашем сайте:</label>
                </th>
                <td>
          http://<?php echo $_SERVER['HTTP_HOST']; ?>/wpsalez/a1sms/<?php echo $a1urlseckey; ?>/
          </td>
</tr> 
<tr>
                <th>
                    <label for="name">Префикс a1:</label>
                </th>
                <td>
                    <input type="text" name="prefix" id="prefix" value="<?php echo $a1prefix; ?>" class="regular-text"> <span class="description"></span>
                </td>
</tr> 
<tr>
                <th>
                    <label for="name">Секретный ключ:</label>
                </th>
                <td>
                    <input type="text" name="seckey" id="seckey" value="<?php echo $a1seckey; ?>" class="regular-text"> <span class="description"></span>
                </td>
</tr>
<tr>
                <th>
                    <label for="name">Email по которому связываться в случае проблем с sms:</label>
                </th>
                <td>
                    <input type="text" name="a1_contact_email" id="a1_contact_email" value="<?php echo $a1_contact_email; ?>" class="regular-text"> <span class="description"></span>
                </td>
</tr>
<tr>
                <th>
                    <label for="a1login">Логин в системе a1:</label>
                </th>
                <td>
                    <input type="text" name="a1login" id="a1login" value="<?php echo $a1login; ?>" class="regular-text"> <span class="description"></span>
                </td>
</tr>
<tr>
                <th>
          </th>
                <td>
                    <input type="submit" name="submit" id="submit" class="button-primary" value="Сохранить" /> 
                </td>
</tr> 
</table>
</form>
