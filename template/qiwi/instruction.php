<!-- Требование киви может меняться -->
<p>
<img src="/wp-content/plugins/wpsalez/static/i/qiwi-logo-vertical.jpg" align="left">
С помощью <a href="https://w.qiwi.ru/" mce_href="https://w.qiwi.ru/" target="_blank">QIWI Кошелька</a> вы можете оплатить наши товары и услуги <strong>
моментально</strong> и <strong>без комиссии</strong>!<br /> <br />Для этого:<br /> <strong>1</strong>. Сформируйте заказ;<br />
<strong>2</strong>. Выберите в качестве оплаты QIWI Кошелек и введите номер своего сотового телефона;<br /> <strong>3</strong>.
Оплатите автоматически созданный счет на оплату: наличными в терминалах QIWI, на сайте
<a href="https://w.qiwi.ru/" mce_href="https://w.qiwi.ru/" target="_blank">QIWI Кошелька</a>
или с помощью приложений для социальных сетей и
<a href="https://w.qiwi.ru/mobile.action" mce_href="https://w.qiwi.ru/mobile.action" target="_blank">мобильных телефонов и планшетов</a>.</p>
<p>QIWI Кошелек легко <a href="http://w.qiwi.ru/fill.action" mce_href="http://w.qiwi.ru/fill.action" target="_blank">пополнить</a>
в терминалах QIWI и партнеров, салонах сотовой связи, супермаркетах, банкоматах или интернет-банк.</p>
<p>Оплатить счет на оплату можно не только со счета QIWI Кошелька, но и банковской картой, наличными,
а также с лицевых счетов мобильных телефонов Билайн, МегаФон и МТС.</p>
<p>Если у вас еще нет QIWI Кошелька – вы можете зарегистрировать его на
<a href="http://w.qiwi.ru/" mce_href="http://w.qiwi.ru/" target="_blank">сайте</a> QIWI Кошелька или в любом из приложений за несколько минут.</p>
<!-- /Требование Киви -->
<ul class="wpsalez_qiwi_action_list_onstring">
<li><a href="#" id="wpsalez_next_buy">Перейти к выставлению счета</a></li>
<li><a href="#" id="wpsalez_cancel_buy">Отказаться от покупки</a></li>
</ul>
   
