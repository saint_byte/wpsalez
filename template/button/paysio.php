<?php
function button_paysio_make($id,$BUTT_IMG_OFF = false,$bill_is_ready = false)
{
 $wpsalez_paysio_publickey     = get_option( 'wpsalez_paysio_publickey');
 if ($bill_is_ready)
 {
  $pay_function = 'wpsalez_paysiobuy('.$id.')';
 }
 else
 {
  $pay_function = 'wpsalez_paysiobill('.$id.')';
 }
 $shtml = '<div class="wpsalez_buybutt wpsalez_buya1paybutton" onclick="'.$pay_function.'">Купить';
 if (!$BUTT_IMG_OFF)
 {
        $shtml .= ' <img src="'.plugins_url().'/wpsalez/static/i/paysio_logo.png" alt="">';
 }
 $shtml .= '</div>';
 $shtml .= '<script>'."\r\n";
 $shtml .= 'Paysio.setPublishableKey(\''.$wpsalez_paysio_publickey.'\');'."\r\n";
 $shtml .= '</script>'."\r\n";
 return $shtml;
}

