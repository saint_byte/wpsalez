<div class="wrap">
<h2>WP Salez / Продукты</h2>
<?php if($err) { ?>
 <div id="message" class="updated below-h2"><p>
 <ul>
 <?php foreach( $err_str as $error ) { ?>
 <li><?php echo $error; ?></li>
 <?php } ?>
 </ul>
 </p></div>
<?php } ?>
<div class="wpsalez_info">
Для того чтоб добавить возможность покупки в вашу страницу или запись вставьте в текст:
<ul>
<li>С полным описанием продукта [wpsalez_product {"id":"<?php echo $id; ?>"}]</li>
<li>Компактный вид [wpsalez_product {"id":"<?php echo $id; ?>","view":"compact"}]</li>
</ul>
</div>
<form method="post" action="">
<input type="hidden" name="id" value="<?php echo $pdata->id; ?>">
<table class="form-table">
<tbody>
<tr>
                <th>
                    <label for="name">Название:</label>
                </th>
                <td>
                    <input type="text" name="name" id="name" value="<?php echo htmlspecialchars($pdata->name); ?>"  class="regular-text"> <span class="description"></span>
                </td>
</tr> 

<tr>
        <th><label for="description">Описание:</label></th>
        <td><textarea name="description" id="description" rows="5" cols="30" style="width: 500px; margin-bottom: 6px;"><?php echo htmlspecialchars($pdata->descr); ?></textarea><br>
        <span class="description"></span></td>
</tr>
<tr>
        <th><label for="cost">Стоимость:</label></th>
        <td><input type="text" name="cost" id="cost" value="<?php echo htmlspecialchars($pdata->cost); ?>" class="regular-text"> <span class="description"></span><br></td>
</tr>

<tr>
        <th><label for="smsnum">Номер на который надо отправить CMC:</label></th>
        <td><input type="text" name="smsnum" id="smsnum" value="<?php echo htmlspecialchars($pdata->smsnum); ?>"  class="regular-text"> <span class="description"></span><br>
        <span class="cost">
            <a href="http://www.a1agregator.ru/main/abonent/">Цена для абонентов</a>
            <a href="https://partner.a1pay.ru/tariff/index">Ваш доход</a>
        </span></td>
</tr>
<!-- -->
<tr>
        <th><label for="smsnum">Тип оплаты:</label></th>
        <td>
        <?if (count($paytypez) > 0) { ?>
        <?if (count($paytypez) > 1) { ?>
        <?php foreach($paytypez as $k=>$type1) { ?>
        <input type="checkbox" name="paytype[]" value="<?php echo $k ?>" id="<?php echo $k ?>" <?php if (in_array($k,$pt_arr)) { echo 'checked="checked"'; } ?>> <label for="<?php echo $k ?>"  ><?php echo $type1['name'] ?></label>&nbsp;
        <?php } ?>
        <?php } else { /* = 1 */ ?>
        <input type="hidden" name="paytype[]" value="<?php echo $k ?>"><?php echo $type1['name'] ?>
        <?php } ?>
        <?php } else { /* cOUNT > 0*/?>
        Так странная штука но товар не будет оплачен - потому нет не одной установленной системы оплаты
        <?php } ?>
        <span class="description"><!--- Какую систему оплаты использовать --></span><br>
        <span class="cost"></span></td>
</tr>
<!-- -->
<tr>
        <th></th>
        <td><input type="submit" name="submit" id="submit" class="button-primary" value="Сохранить"></td>
</tr>
</tbody>
</table>
</form>
