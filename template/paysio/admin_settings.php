<?php /*
Название:
URL страницы, на которой предлагается оплатить услугу:
URL скрипта обработчика на вашем сайте:
URL дополнительного скрипта обработчика:
Тип сервиса:
Префиксы:
Секретный ключ:
Кодировка ответа сервиса:

https://partner.a1pay.ru/service/index
*/?>
<div class="wrap">
<h2>WP Salez / Настройка Pays.io</h2>
<form method="Post">
<table class="form-table">
<tbody>
<tr>
                <th>
                    <label for="name">URL страницы, на которой предлагается оплатить услугу:</label>
                </th>
                <td>
          http://<?php echo $_SERVER['HTTP_HOST']; ?>/wpsalez/products_list
          </td>
</tr> 
<tr>
                <th>
                    <label for="name">URL скрипта обработчика на вашем сайте:</label>
                </th>
                <td>
          http://<?php echo $_SERVER['HTTP_HOST']; ?>/wpsalez/paysio/<?php echo $a1urlseckey; ?>/
          </td>
</tr> 
<tr>
                <th>
                    <label for="name">Открытый ключ:</label>
                </th>
                <td>
                    <input type="text" name="wpsalez_paysio_publickey" id="wpsalez_paysio_publickey" value="<?php echo $wpsalez_paysio_publickey; ?>" class="regular-text"> <span class="description"></span>
                </td>
</tr> 
<tr>
                <th>
                    <label for="name">Секретный ключ:</label>
                </th>
                <td>
                    <input type="text" name="wpsalez_paysio_secretkey" id="wpsalez_paysio_secretkey" value="<?php echo $wpsalez_paysio_secretkey; ?>" class="regular-text"> <span class="description"></span>
                </td>
</tr> 

<tr>
                <th>
          </th>
                <td>
                    <input type="submit" name="submit" id="submit" class="button-primary" value="Сохранить" /> 
                </td>
</tr> 
</table>
</form>
