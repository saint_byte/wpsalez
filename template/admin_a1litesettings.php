<?php /*
 Название: В качестве продавца будет выводится текст, указанный в этом поле
URL скрипта обработчика на Вашем сайте: Скрипт, которому передаётся информация о принятых платежах от системы A1Pay.
Получать дополнительные параметры оплаты
* URL страницы успешной покупки: Например: http://www.test.ru/success.html
* URL страницы ошибки: Например: http://www.test.ru/error.html
Секретный ключ: Придумайте и укажите ключ, по которому в скрипте-обработчике проверяется достоверность передаваемых данных. Узнать подробнее
Email:
*/?>
<div class="wrap">
<h2>WP Salez / Настройка a1lite</h2>
<form method="Post">
<table class="form-table">
<tbody>
<tr>
                <th>
                    <label for="name">Название:</label>
                </th>
                <td>
         Ваше название
          </td>
</tr>
<tr>
                <th>
                    <label for="name">URL скрипта обработчика на Вашем сайте:</label>
                </th>
                <td>
          http://<?php echo $_SERVER['HTTP_HOST']; ?>/wpsalez/a1lite/<?php echo $a1urlseckey; ?>/
          </td>
</tr>
<tr>
                <th>
                    <label for="name">URL страницы успешной покупки:</label>
                </th>
                <td>
          http://<?php echo $_SERVER['HTTP_HOST']; ?>/wpsalez/a1lite_success/
          </td>
</tr>
<tr>
                <th>
                    <label for="name">URL страницы ошибки:</label>
                </th>
                <td>
          http://<?php echo $_SERVER['HTTP_HOST']; ?>/wpsalez/a1lite_fail/
          </td>
</tr>
<tr>
                <th>
                    <label for="name">Email по которому связываться в случае проблем:</label>
                </th>
                <td>
                    <input type="text" name="a1_contact_email" id="a1_contact_email" value="<?php echo $a1_contact_email; ?>" class="regular-text"> <span class="description"></span>
                </td>
</tr>
<tr>
                <th>
                    <label for="name">Secret Key:</label>
                </th>
                <td>
                    <input type="text" name="wpsalez_a1litesecretkey" id="wpsalez_a1litesecretkey" value="<?php echo $wpsalez_a1litesecretkey; ?>" class="regular-text"> <span class="description"></span>
                </td>
</tr>
<tr>
                <th>
                    <label for="a1login">Код:<small></small></label>
                </th>
                <td>
                    <textarea name="keydata" style="width: 305px;" rows="7" cols="35"></textarea><br />
                                <span class="description">Текущий ключ: <?php if (!empty($a1litekey)){ echo $a1litekey; } else { echo '<span style="color: #F00; font-weight: bold">НЕ ЗАДАН</span>';} ?></span>
                </td>
</tr>
<tr>
                <th>
                    <label for="a1login">Логин в системе a1:</label>
                </th>
                <td>
                    <input type="text" name="a1login" id="a1login" value="<?php echo $a1login; ?>" class="regular-text"> <span class="description"></span>
                </td>
</tr>

<tr>
                <th>
          </th>
                <td>
                    <input type="submit" name="submit" id="submit" class="button-primary" value="Сохранить" />
                </td>
</tr>
</table>
</form>
