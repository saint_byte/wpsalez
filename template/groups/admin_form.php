<script>
function reload_to_remove()
{
jQuery( "#sort li" ).unbind().dblclick(function() {
        remove_item(this);
        reload_to_remove();
});
}
function remove_item(obj)
{
        jQuery(obj).remove();
        get_ids();
}
function add_item(obj)
{
        jQuery( "#sort" ).append(jQuery('<li data-id="'+jQuery(obj).attr('data-id')+'"><span>'+jQuery(obj).text()+'</span></li>'));
        reload_to_remove();
        get_ids();
}

function get_ids()
{
 var s ='';
 jQuery('#sort li').each(function(index) {
        var id = jQuery(this).attr('data-id');
        if (id) {
                s += id+',';
        }
 });
 jQuery("#data").val(s);
}



jQuery(document).ready(function() {
        jQuery('#prds_list .wpsalez_groups_products_add_item').dblclick(function() {
                var id = jQuery(this).attr('data-id');
                add_item(this);
        });
        jQuery( "#sort" ).sortable({
                update: function(event, ui) { get_ids(); }
        });
        jQuery( "#sort" ).disableSelection();
        jQuery( "#data" ).click(function(event) {
                event.stopPropagation();
                event.preventDefault();
                get_ids();
        });
        reload_to_remove();
        get_ids();
});

</script>
<style>
        .wpsalez_groups_products_msg
        {
        border: 1px solid #8A8A8A;
        -webkit-border-radius: 3px;
        -moz-border-radius: 3px;
        border-radius: 3px;
        font-size:12px;
        padding: 6px;
        background-color: #F8F5AA;
        
        }
        .wpsalez_groups_bg_red
        {
        margin: 5px;
        }
        .wpsalez_groups_sort_list { list--style-type: none; margin: 0; padding: 0; width: 60%; cursor: pointer; }
        .wpsalez_groups_sort_list li { margin: 0 3px 3px 3px; padding: 0.4em; padding-left: 1.5em; font-size: 1.4em; height: 18px; }
        .wpsalez_groups_sort_list li span { position: absolute; margin-left: -1.3em; border: 1px solid black; -webkit-border-radius: 3px;
-moz-border-radius: 3px;
border-radius: 3px;
padding: 5px; min-width: 50%;
        }
        .wpsalez_groups_products_add_item {
                cursor: pointer;
                 border: 1px solid black;
                 -webkit-border-radius: 3px;
                -moz-border-radius: 3px;
                border-radius: 3px;
                padding: 5px;
        }
        .wpsalez_groups_products_add_remove_item { cursor: pointer; }
</style>
<div class="wrap">
<h2>WP Salez / Группы / <?php echo($_GET['id'] ? 'Редактирование':'Добавление'); ?></h2>
<form method="post">
<?php if ($errors) { ?>
<div class="wpsalez_groups_products_msg error ">
<ul>
<?php  foreach ($errors as $err) { ?>
<li><?php echo $err; ?></li>
<?php } ?>
</ul>
</div>
<?php } ?>
Название: <input type="text" name="name" value="<?php echo @$name; ?>">
<table width="100%">
<tr>
<td width="20%" valign="top">
<div id="add-custom-links" class="postbox ">
<div class="handlediv" title="Продукты"><br></div><h3 class="hndle">Продукты</h3>
<div class="inside">
<div class="customlinkdiv" id="customlinkdiv">
<?php if (count($prds) > 0 ) {?>
<ul id="prds_list" >
<?php foreach($prds as $pr) { ?>
<li data-id="<?php echo $pr->id; ?>" class="wpsalez_groups_products_add_item"><?php echo $pr->name; ?> </li>
<?php } ?>
</ul>
<br />
<br />
<div class="wpsalez_groups_products_msg">
Кликни дважды по продукту чтоб добавить его в группу
</div>
<?php } else { ?>
<a href="admin.php?page=pg&action=add">Добавьте хотя бы один продукт</a>
<?php } ?>
</div><!-- /.customlinkdiv -->
</div>
</div>
</td>
<td  valign="top">
<div id="add-custom-links" class="postbox ">
<div class="handlediv" title="Группа"><br></div><h3 class="hndle"><span>Группа</span></h3>
<div class="inside">
<div class="customlinkdiv" id="customlinkdiv">
<ul  id="sort" class="wpsalez_groups_sort_list">
<?php if ($addprs)  { ?>
<?php foreach($addprs as $prd) { ?>
<li data-id="<?php echo $prd->id; ?>"><span><?php echo $prd->name; ?></span></li>
<?php } ?>
<?php } ?>
</ul>
<br />
<br />
<div class="wpsalez_groups_products_msg">
Кликни дважды по продукту чтоб убрать его из группы. Порядок продуктов можно сортировать перетаскивание продуктов вверх/низ списка
</div>
</div><!-- /.customlinkdiv -->
</div>
</div>
</td>
</tr>
</table>
 <input type="hidden" name="data"  id="data" value="">
<input type="submit" name="submit" id="submit" class="button-primary" value="Сохранить">
</form>
