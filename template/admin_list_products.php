<div class="wrap">
<h2>WP Salez / Продукты</h2>
<form>
<input type="hidden" name="page" value="pg">
<input type="hidden" name="action" value="add">
<input type="submit" name="submit" id="submit" class="button-primary" value="Добавить">
</form>
<br class="clear">
<table class="wp-list-table widefat fixed pages" cellspacing="0">
        <thead>
        <tr>
                <th scope="col" id="cb" class="manage-column column-cb check-column" style=""></th>
                <th scope="col" id="title" class="manage-column column-title" style="">Товар</th>
          </tr>
        </thead>
        <tbody id="the-list">
        <?php foreach($products as $product) { ?>
                                <tr class="post-117 page type-page status-publish hentry alternate iedit author-self" valign="top">
                                <th scope="row" class="check-column"></th>
                                <td class="post-title page-title column-title">
                                <strong><a class="row-title" href="<?php echo $_SERVER['REQUEST_URI'].'&action=edit&id='.$product->id; ?>" title="Редактировать <?php echo $product->name; ?>"><?php echo $product->name; ?></a></strong>
                                <div class="row-actions">
                                <span class="edit"><a href="<?php echo $_SERVER['REQUEST_URI'].'&action=edit&id='.$product->id; ?>" title="Редактировать этот элемент">Изменить</a> | </span>
                                <span class="trash"><a class="submitdelete" title="Удалить" onclick="return confirm('Удалить???');" href="<?php echo $_SERVER['REQUEST_URI'].'&action=delete&id='.$product->id; ?>">Удалить</a>
                                </div>
                                </td>
                        </tr>
         <?php } //foreach
         ?>
          </tbody>
</table>
</div>
