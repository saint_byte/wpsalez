<?php
if (ENABLE_GROUPS)
{
$table = $wpdb->prefix.'wpsalez_groups';
$sql1='
CREATE TABLE IF NOT EXISTS `'.$table.'` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `date` datetime NOT NULL COMMENT "дата создания",
  `name` varchar(128) COLLATE utf8_unicode_ci NOT NULL COMMENT "Название группы",
  `products` text COLLATE utf8_unicode_ci NOT NULL COMMENT "продукты",
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1' ;
$wpdb->query($sql1);
}
/*

  `msg_trans` varchar(255) COLLATE utf8_unicode_ci NOT NULL COMMENT "транслитерированное сообщение",
  `clear_msg` text COLLATE utf8_unicode_ci NOT NULL,
  `operator` varchar(255) COLLATE utf8_unicode_ci NOT NULL COMMENT "id оператора",
  `operator_id` int(10) unsigned NOT NULL COMMENT "id оператора",
  `user_id` bigint(20) unsigned NOT NULL COMMENT "номер абонента",
  `smsid` int(10) unsigned NOT NULL COMMENT "id смс",
  `cost_rur` float(15,4) unsigned NOT NULL COMMENT "сумма в рублях",
  `cost` float(15,4) unsigned NOT NULL COMMENT "информационный параметр (по курсу последней выплаты)",
  `test` tinyint(1) unsigned NOT NULL COMMENT "1 - тестовая смс",
  `num` varchar(20) COLLATE utf8_unicode_ci NOT NULL COMMENT "короткий номер",
  `skey` char(32) COLLATE utf8_unicode_ci NOT NULL COMMENT "секретный ключ md5",
  `sign` char(32) COLLATE utf8_unicode_ci NOT NULL COMMENT "md5 от параметров",
  `ran` tinyint(1) unsigned NOT NULL,
  `answer` text COLLATE utf8_unicode_ci NOT NULL COMMENT "ответ сервиса без заголовка",
  `uid` int(11) NOT NULL COMMENT "ИД пользователя местной системы",
  `ip` varchar(128) COLLATE utf8_unicode_ci NOT NULL COMMENT "IP адрес",
  `qs` text COLLATE utf8_unicode_ci NOT NULL,
*/



