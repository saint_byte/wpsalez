<?php
function wpz_groups()
{
$G = new Groups();
$P = new Products();
$action = $_GET['action'];
if ($action == 'add' )
{
                wp_enqueue_script( 'jquery-ui-draggable' );
                wp_enqueue_script( 'jquery-ui-droppable' );
                wp_enqueue_script( 'jquery-ui-sortable' );
        // Metaboxes
                wp_enqueue_script( 'common' );
                wp_enqueue_script( 'wp-lists' );
                wp_enqueue_script( 'postbox' );
                
        $prds = $P->getAllProducts();
        if ($_POST)
        {
        $name = $_REQUEST['name'];
        if (empty($name))
        {
                $errors[] = 'Заполните поле Название';
        }
        $data = $_REQUEST['data'];
        if (empty($data))
        {
                $errors[] = 'Добавьте в группу хоть один товар';
        }
        if (count($errors) ==0)
        {
         //Ошибок нет сохраняем
                $data_arr = explode(',',$data);

                $G->add($name,$data_arr);
                include(dirname(__FILE__).'/../template/groups/admin_form_ok.php');
         
        } else
        {
                include(dirname(__FILE__).'/../template/groups/admin_form.php');
        }
        } else {

                include(dirname(__FILE__).'/../template/groups/admin_form.php');
        }
}
elseif ($action == 'view')
{
        $id = intval($_GET['id']);
                wp_enqueue_script( 'jquery-ui-draggable' );
                wp_enqueue_script( 'jquery-ui-droppable' );
                wp_enqueue_script( 'jquery-ui-sortable' );
        // Metaboxes
                wp_enqueue_script( 'common' );
                wp_enqueue_script( 'wp-lists' );
                wp_enqueue_script( 'postbox' );
        $prds = $P->getAllProducts();
        if ($_POST)
        {
                $name = $_REQUEST['name'];
        if (empty($name))
        {
                $errors[] = 'Заполните поле Название';
        }
        $data = $_REQUEST['data'];
        if (empty($data))
        {
                $errors[] = 'Добавьте в группу хоть один товар';
        }
        if (count($errors) ==0)
        {
                $data_arr = explode(',',$data);
                $G->save($id,$name,$data_arr);
                include(dirname(__FILE__).'/../template/groups/admin_form_ok.php');
        }
        else
        {
        }
        }
        else
        {
          $data = $G->getById($id);
          $name = $data->name;
          //----------------------------
          $addprs = array();
          foreach($data->products['products'] as $pitem)
          {
           $pp = $P->getById($pitem);
           if ($pp)
           {
                $addprs[] = $pp;
           }
          }

          //----------------------------
          include(dirname(__FILE__).'/../template/groups/admin_form.php');
        }
}
else
{
        $grps = $G->getAll();
        include(dirname(__FILE__).'/../template/groups/admin_list.php');
}
}

