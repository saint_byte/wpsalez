<?php
/*
Plugin Name: WpSaleZ        
Plugin URI: http://DotBlog.ru
Description: Make sale with you WordPress
Version: 1.0
Author: Saint_Byte
Author URI: http://dotblog.ru
License: Commersion
*/
define('INCLIDE_A1LITE',true);
define('INCLIDE_A1SMS',true);
define('INCLIDE_VIDEOSELL',true);
define('INCLIDE_QIWI',true);
define('INCLIDE_PAYSIO',true);

define('REMOVE_MAGIC_QUOTES',true);
define('USE_GOOGLE_CDN_IF_POSSIBLE',true);
define('ENABLE_BILLS',true);
define('ENABLE_GROUPS',true);
define('ENABLE_CART',true);

if (!session_id()) { @session_start();}

if (!defined('ABSPATH')) { die(); }

require_once dirname(__FILE__) . '/inc/func.php';

require_once dirname(__FILE__) . '/classes/paytypes.php';
require_once dirname(__FILE__) . '/classes/products.php';
require_once dirname(__FILE__) . '/classes/bills.php';
require_once dirname(__FILE__) . '/classes/a1bills.php';
//-------------------------------------------------------------------------
//add_submenu_page( $parent_slug, $page_title, $menu_title, $capability, $menu_slug, $function );
$SUBMENU = array();
$SUBMENU[] = array(
                    'title'      => 'WP SaleZ',
                    'menu'       => 'Продукты',
                    'capability' => 8,
                    'menu_slug'  => 'pg',
                    'fnc'        => 'wpz_pg'
                  );
if (ENABLE_BILLS) {
        $SUBMENU[] =array(
                'title'      => 'WP SaleZ',
                'menu'       => 'Счета',
                'capability' => 8,
                'menu_slug'  => 'bl',
                'fnc'        => 'wpz_bl'
        );
}
if (ENABLE_GROUPS) {
        $SUBMENU[] =array(
                'title'      => 'WP SaleZ',
                'menu'       => 'Группы',
                'capability' => 8,
                'menu_slug'  => 'groups',
                'fnc'        => 'wpz_groups'
        );
}
if (INCLIDE_A1SMS) {
        $SUBMENU[] = array(
                'title'      =>        'WP SaleZ',
                'menu'       =>        'Настройка a1sms',
                'capability' =>        8,
                'menu_slug'  =>        'a1settings',
                'fnc'        =>        'wpz_a1settings'
        );
        $SUBMENU[] = array(
               'title'      => 'WP SaleZ',
               'menu'       => 'Оплаты по a1sms',
               'capability' => 8,
               'menu_slug'  => 'a1bills',
               'fnc'        => 'wpz_a1bills'
         );

}  // if INCLUDE_A1sms
if (INCLIDE_A1LITE) {

    $SUBMENU[] = array(
                    'title'      => 'WP SaleZ',
                    'menu'       => 'Настройка a1lite',
                    'capability' => 8,
                    'menu_slug'  => 'a1litesettings',
                    'fnc'        => 'wpz_a1litesettings'
                  );
    $SUBMENU[] = array(
                   'title'      =>'WP SaleZ',
                   'menu'       =>'Оплаты по a1lite',
                   'capability' =>8,
                   'menu_slug'  =>'a1litebills'   ,
                   'fnc'        =>'wpz_a1litebills'
    );
}

//-------------------------------------------------------------------------
if (INCLIDE_A1LITE) 
{
        require_once dirname(__FILE__) . '/classes/a1lite_pays.php';
}
if (INCLIDE_VIDEOSELL)
{
        require_once dirname(__FILE__) . '/videosell/admin.php';
        require_once dirname(__FILE__) . '/videosell/main.php';
}
if (INCLIDE_QIWI)
{

        require_once dirname(__FILE__) . '/classes/qiwi_request.php';
        require_once dirname(__FILE__) . '/qiwi/admin.php';
        require_once dirname(__FILE__) . '/qiwi/main.php';
}
if (INCLIDE_PAYSIO)
{

        require_once dirname(__FILE__) . '/paysio/admin.php';
        require_once dirname(__FILE__) . '/paysio/main.php';
}


if (ENABLE_GROUPS)
{
                 require_once dirname(__FILE__) . '/classes/groups.php';
                 require_once dirname(__FILE__) . '/groups/admin.php';
                 require_once dirname(__FILE__) . '/groups/main.php';
}
if (ENABLE_CART)
{
        require_once dirname(__FILE__) . '/classes/cart.php';
        require_once dirname(__FILE__) . '/cart/cart.php';
}
require_once dirname(__FILE__) . '/inc/main.php';
require_once dirname(__FILE__) . '/inc/admin.php';

if (REMOVE_MAGIC_QUOTES) 
{
        fix_magic_quotes_gpc();
}


register_activation_hook( __FILE__, 'install' );



function install(){
        require_once dirname(__FILE__) . '/inc/install.php';
        if (INCLIDE_QIWI)
        {
                 require_once dirname(__FILE__) . '/qiwi/install.php';
        }
        if (ENABLE_GROUPS)
        {
                 require_once dirname(__FILE__) . '/groups/install.php';
        }
        
        if (ENABLE_PAYSIO)
        {
                 require_once dirname(__FILE__) . '/paysio/install.php';
        }
        

}

//-------------------------------------
// Enable Session

function init_sessions() {
    if (!session_id()) {         session_start();     }
        $GLOBALS['SESSION'] = $_SESSION;
}

function save_sessions()
{
        $_SESSION = $GLOBALS['SESSION'];
}

add_action('init', 'init_sessions');
add_action('shutdown', 'save_sessions');
//-------------------------------------
// Hide Wpsalez pages
if ( get_option( 'wpsalez_hide_page') ) {
        add_filter('wp_list_pages_excludes', wpsalez_hide_pages_wp_list_pages_excludes);
}


function wpsalez_hide_pages_wp_list_pages_excludes($exclude_array)
{
        global $wpdb;
        $sql = "SELECT ID FROM ".$wpdb->posts." WHERE post_name ='wpsalez'";
        $id_array = $wpdb->get_col($sql);
        $exclude_array=array_merge($id_array, $exclude_array);
        $sql = "SELECT ID FROM ".$wpdb->posts." WHERE post_parent = '".$id_array[0]."' ";
        $id_array = $wpdb->get_col($sql);
        $exclude_array=array_merge($id_array, $exclude_array);
        return $exclude_array;
}
//-------------------------------------
