function validateEmail(email) {
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}
function validateTel(tel)
{
if (tel.length!=10) return false;
var re = /^[0-9]*$/;
return re.test(tel);
}
/* WP_SALEZ_CLASSES */
var ir;
var Wpsalez_DarkBox = {
init: function() {
var zindex = 10
if (jQuery('#wpsalez_darkbox')) {         jQuery('#wpsalez_darkbox').remove(); }
var darkbox = jQuery('<div id="wpsalez_darkbox"></div>');
darkbox.attr("class","wpsalez_opacity50");
darkbox.css({
                               'z-index':zindex,
                               'height':jQuery(document).height()+'px'
                                });
jQuery(document.body).append(darkbox);
//darkbox.click(function() {if (jQuery('#wpsalez_darkbox')) {         $('#wpsalez_darkbox').remove(); }} );
},

close: function()
{
if (jQuery('#wpsalez_darkbox')) {         jQuery('#wpsalez_darkbox').remove();}
}
};
var Wpsalez_Window ={
width: '350',
height: '120',
wnd: null,
init: function()
{
var zindex = 20;
this.wnd = jQuery('<div id="wpsalez_window" class="wpsalez_window"></div>');
var top = jQuery(window).scrollTop() + (jQuery(window).height() * 30 / 100);
this.wnd.css({'z-index':zindex,'top':top+'px'});
this.wnd.html('123123');
jQuery(document.body).append(this.wnd);
},
loadshow: function()
{
  if (this.wnd == null)
  {
    this.init();
  }
  this.wnd.html('<div class="wpsalez_loading"></div>');
},
loadshow2: function(s)
{
var zindex = 20;
this.height = 80;
this.width = 350;
this.wnd = jQuery('<div id="wpsalez_window" class="wpsalez_window"></div>');

var top = jQuery(window).scrollTop() + (jQuery(window).height() * 30 / 100);
this.wnd.css({'z-index':zindex,'top':top+'px','min-height':this.height+'px'});
this.wnd.width(this.width);
this.wnd.height(this.height);
this.wnd.html(s+'<br />'+'<div class="wpsalez_loading2"></div>');
jQuery(document.body).append(this.wnd);
},

close: function() {
if (jQuery('#wpsalez_window')) { jQuery('#wpsalez_window').remove();}
}
}


/* WPSALEZ */
function wpsalez_buy(product_id)
{
        Wpsalez_DarkBox.init();
        Wpsalez_Window.init();
        Wpsalez_Window.loadshow2();
        jQuery.ajax({
                url: "/wpsalez/product/?id="+product_id,
                success: function(data){
                        jQuery('#wpsalez_window').html(data);
                        jQuery('#wpsalez_result').html('Ожидание оплаты <div class="wpsalez_small_loading"> </div>');
                        jQuery('#wpsalez_cancel_buy').click(function() {
                                                                window.clearInterval(ir);
                                Wpsalez_DarkBox.close();
                                Wpsalez_Window.close();
                        });
                        ir = window.setInterval(getPayStatus,1000);
                }
          });
        
        
        
}
function wpsalez_paybill(product_id)
{
        Wpsalez_DarkBox.init();
        Wpsalez_Window.init();
        Wpsalez_Window.loadshow();
        jQuery.ajax({
                url: "/wpsalez/product/?id="+product_id,
                success: function(data){
                        jQuery('#wpsalez_window').html(data);
                        jQuery('#wpsalez_result').html('Ожидание оплаты <div class="wpsalez_small_loading"> </div>');
                        jQuery('#wpsalez_cancel_buy').click(function() {
                                                                window.clearInterval(ir);
                                Wpsalez_DarkBox.close();
                                Wpsalez_Window.close();
                        });
                        ir = window.setInterval(getPayStatus,1000);
                }
          });



}
function getPayStatus()
{
jQuery.ajax({
                url: "/wpsalez/bill_payed/?id="+bill_id,
                success: function(data){
                        if (data != '') {
                        jQuery('#wpsalez_result').html(data);
                        jQuery('#wpsalez_cancel_buy').html('');
                        window.clearInterval(ir);
                        }

                }
          });
}
function wpsalez_a1litebuy(product_id)
{
Wpsalez_DarkBox.init();
Wpsalez_Window.loadshow2('Перенаправление на страницу оплаты, подождите');
window.location.href="/wpsalez/allite_product/?id="+product_id;
}
function wpsalez_a1litepaybill(bill_id)
{
Wpsalez_DarkBox.init();
Wpsalez_Window.loadshow2('Перенаправление на страницу оплаты, подождите');
window.location.href="/wpsalez/allite_product/?billid="+bill_id;
}
// --------------------------------------------------------------
//
//                QIWI
//
// ---------------------------------------------------------------
// Оплата уже выставленно счета
function wpsalez_qiwipaybill(bill_id)
{
        Wpsalez_DarkBox.init();
        Wpsalez_Window.init();
        Wpsalez_Window.loadshow();
        jQuery.ajax({
                url: "/wpsalez/qiwi/instruction/?id="+bill_id,
                success: function(data){
                        jQuery('#wpsalez_window').html(data);
                        jQuery('#wpsalez_next_buy').click(function() {
                                        wpsalez_qiwipaybill(bill_id);
                        });
                        jQuery('#wpsalez_cancel_buy').click(function() {
                                Wpsalez_DarkBox.close();
                                Wpsalez_Window.close();
                        });
                }
          });
}
function  wpsalez_qiwipaybill(bill_id)
{
 Wpsalez_Window.loadshow();
 jQuery.ajax({
                url: "/wpsalez/qiwi/details_form/?id="+bill_id,
                success: function(data){
                        jQuery('#wpsalez_window').html(data);
                        jQuery('#wpsalez_end_button').click(function() {
                                        wpsalez_qiwipaybill2(bill_id);
                        });
                        jQuery('#wpsalez_cancel_buy').click(function() {
                                Wpsalez_DarkBox.close();
                                Wpsalez_Window.close();
                        });
                }
          });
}
function  wpsalez_qiwipaybill2(bill_id)
{
  var email = jQuery('#wpsalez_qiwi_email').val();
  var tel   = jQuery('#wpsalez_qiwi_tel').val();
  if (!validateEmail(email))
  {
    alert('Введите правильно email');
    jQuery('#wpsalez_qiwi_email').focus();
    return false;
  }
  if (!validateTel(tel))
  {
    alert('Введите правильно номер телефона');
    jQuery('#wpsalez_qiwi_tel').focus();
    return false;
  }
   Wpsalez_Window.loadshow();
   jQuery.ajax({
                url: "/wpsalez/qiwi/end/?id="+product_id+'&email='+email+'&tel='+tel,
                success: function(data){
                        jQuery('#wpsalez_window').html(data);
                        jQuery('#wpsalez_cancel_buy').click(function() {
                                Wpsalez_DarkBox.close();
                                Wpsalez_Window.close();
                        });
                }
          });
}
// Qiwi высталение и оплата счета
function wpsalez_qiwibuy(product_id)
{
        Wpsalez_DarkBox.init();
        Wpsalez_Window.init();
        Wpsalez_Window.loadshow();
        jQuery.ajax({
                url: "/wpsalez/qiwi/instruction/?id="+product_id,
                success: function(data){
                        jQuery('#wpsalez_window').html(data);
                        jQuery('#wpsalez_next_buy').click(function() {
                                        wpsalez_qiwibuy1(product_id);
                        });
                        jQuery('#wpsalez_cancel_buy').click(function() {
                                Wpsalez_DarkBox.close();
                                Wpsalez_Window.close();
                        });
                }
          });



}
function  wpsalez_qiwibuy1(product_id)
{
 Wpsalez_Window.loadshow();
 jQuery.ajax({
                url: "/wpsalez/qiwi/details_form/?id="+product_id,
                success: function(data){
                        jQuery('#wpsalez_window').html(data);
                        jQuery('#wpsalez_end_button').click(function() {
                                        wpsalez_qiwibuy2(product_id);
                        });
                        jQuery('#wpsalez_cancel_buy').click(function() {
                                Wpsalez_DarkBox.close();
                                Wpsalez_Window.close();
                        });
                }
          });
}
function  wpsalez_qiwibuy2(product_id)
{
  var email = jQuery('#wpsalez_qiwi_email').val();
  var tel   = jQuery('#wpsalez_qiwi_tel').val();
  if (!validateEmail(email))
  {
    alert('Введите правильно email');
    jQuery('#wpsalez_qiwi_email').focus();
    return false;
  }
  if (!validateTel(tel))
  {
    alert('Введите правильно номер телефона');
    jQuery('#wpsalez_qiwi_tel').focus();
    return false;
  }
   Wpsalez_Window.loadshow2();
   jQuery.ajax({
                url: "/wpsalez/qiwi/end/?id="+product_id+'&email='+email+'&tel='+tel,
                success: function(data){
                        jQuery('#wpsalez_window').html(data);
                        jQuery('#wpsalez_cancel_buy').click(function() {
                                Wpsalez_DarkBox.close();
                                Wpsalez_Window.close();
                        });
                }
          });
}
// --------------------------------------------------------------
//
//                CART
//
// ---------------------------------------------------------------
function add2cart(event, id)
{
   event = event || window.events
   jQuery(event.target).text('Добавляю...');
   var that = event.target;
   jQuery.ajax({
                url: "/wpsalez/add2cart/?id="+id+'',
                success: function(data){
                        jQuery(that).unbind();
                        jQuery(that).text(data);
                        }

          });
}
function wpsalez_delete_form_cart(id)
{
   event = event || window.events
   jQuery(event.target).text('Удаляю...');
   var that = event.target;
   jQuery.ajax({
                url: "/wpsalez/deleteformcart/?id="+id+'',
                success: function(data){
                        jQuery(that).unbind();
                        jQuery(that).parent().parent().remove();
                        wpsalez_cart_itogo();
                        }

          });
}
function wpsalez_cart_itogo()
{
var wpsalez_cart_sum = 0;
jQuery('.wpsalez_cart_cost').each(function()
{
 wpsalez_cart_sum = wpsalez_cart_sum+parseInt(jQuery(this).text());
 jQuery('.wpsalez_cart_cost_itogo').text(wpsalez_cart_sum);
}

);
}
//-----------
function wpsalez_cart_bill()
{
 var that = event.target;
 jQuery.ajax({
                url: "/wpsalez/cartbill/",
                success: function(data){
                                 jQuery(that).unbind();
                                jQuery(that).text(data);
                        }

          });
}
// --------------------------------------------------------------
//
//                ROBOKASSA
//
// ---------------------------------------------------------------
function wpsalez_robokassabuy(product_id)
{
Wpsalez_DarkBox.init();
Wpsalez_Window.loadshow2('Перенаправление на страницу оплаты, подождите');
window.location.href="/wpsalez/robokassa_product/?id="+product_id;
}
// --------------------------------------------------------------
//
//               Z-PAYMENT
//
// ---------------------------------------------------------------
function wpsalez_zpaymentbuy(product_id)
{
Wpsalez_DarkBox.init();
Wpsalez_Window.loadshow2('Перенаправление на страницу оплаты, подождите');
window.location.href="/wpsalez/zpayment_product/?id="+product_id;
}
// --------------------------------------------------------------
//
//            PAYSIO
//
//---------------------------------------------------------------
//
function wpsalez_paysiobill(product_id)
{
	//alert('['+product_id);
        Wpsalez_DarkBox.init();
        Wpsalez_Window.init();
        Wpsalez_Window.loadshow();
	jQuery.getJSON( "/wpsalez/paysio/createbill/?id="+product_id,  function(data){
				Paysio.form.build(jQuery('#wpsalez_window'), { amount: data.amount,charge_id:data.bill_id  });
                                // jQuery(that).unbind();
                                //jQuery(that).text(data);
                        //}

         });

        //jQuery('#wpsalez_window').html(data);
	

}
